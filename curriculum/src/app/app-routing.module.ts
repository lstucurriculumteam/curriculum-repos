import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PageComponent } from './page/page.component';
import { UsersComponent } from './administration/users/users.component';
import { SpecializationComponent } from './specialization/specialization.component';
import { CurriculumComponent } from './curriculum/curriculum.component';
import { CurriculumListComponent } from './curriculum/curriculum-list/curriculum-list.component';
import { CurriculumContentComponent } from './curriculum/curriculum-content/curriculum-content.component';
import {AccessGuard} from './auth/access.guard';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'admin', component: PageComponent, canActivate: [AccessGuard], children: [
    { path: '', component: UsersComponent }
  ]},
  { path: 'specialization', component: PageComponent, canActivate: [AccessGuard], children: [
    { path: '', component: SpecializationComponent}
  ]},
  { path: 'curriculum', component: PageComponent, canActivate: [AccessGuard], children: [
    { path: '', component: CurriculumComponent, children: [
      { path: '', component: CurriculumListComponent},
      { path: 'content/:id', component: CurriculumContentComponent}
    ]}
  ]},
  { path: '**', redirectTo: '/login'}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}



