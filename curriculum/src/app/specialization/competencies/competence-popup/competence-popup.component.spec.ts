import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompetencePopupComponent } from './competence-popup.component';

describe('CompetencePopupComponent', () => {
  let component: CompetencePopupComponent;
  let fixture: ComponentFixture<CompetencePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompetencePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompetencePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
