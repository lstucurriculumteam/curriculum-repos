import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Competence} from '../../../services/types';
import {DataService} from '../../../services/data.service';

@Component({
  selector: 'app-competence-popup',
  templateUrl: './competence-popup.component.html',
  styleUrls: ['./competence-popup.component.css']
})
export class CompetencePopupComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<CompetencePopupComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private dataService: DataService) { }

  ngOnInit() {
  }

  onSave(){
    this.dataService.addCompetencies(this.data.competence);
    this.dialogRef.close();
  }

}
