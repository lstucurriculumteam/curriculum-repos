import { Component, OnInit, ViewChild, Input } from '@angular/core';
import {MatTableDataSource, MatSort, MatDialog} from '@angular/material';
import { CompetencePopupComponent } from './competence-popup/competence-popup.component';
import {Specialization, Competence, Profile, EducationForm} from '../../services/types';
import {DataService} from '../../services/data.service';

@Component({
  selector: 'app-competencies',
  templateUrl: './competencies.component.html',
  styleUrls: ['./competencies.component.css']
})
export class CompetenciesComponent implements OnInit {

  @Input() specialization: Specialization;
  @Input() profile: Profile;
  @Input() eduForm: EducationForm;
  @Input() isSpec: boolean;

  competencies: Competence[];

  displayedColumns: string[] = ['code', 'name', 'description', 'controls'];
  dataSource = new MatTableDataSource(this.competencies);

  constructor(public dialog: MatDialog,
              private dataService: DataService) { }

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit(){
     this.reset();
     this.dataSource.sort = this.sort;
   }

  updateMandatory(spec: Specialization){
    this.specialization = spec;
    this.reset();
  }

  update(spec: Specialization, profile: Profile, eduForm: EducationForm){
    this.specialization = spec;
    this.profile = profile;
    this.eduForm = eduForm;
    this.reset();
  }

   applyFilter(filterValue: string) {
     this.dataSource.filter = filterValue.trim();
   }

   reset(){
      if (this.profile && this.eduForm){
         this.competencies = this.dataService.getCompetencies(this.specialization.id, this.profile.id, this.eduForm.id);
      }
      else {
         this.competencies = this.dataService.getMandatoryCompetencies(this.specialization.id);
      }
      this.dataSource.data = this.competencies;
   }

   onAddCompetence(){
     const dialogRef = this.dialog.open(CompetencePopupComponent, { data: {title: "Создание новой компетенции",
                                                            competence: new Competence(true, "", "", "", this.specialization.id)}});

     dialogRef.afterClosed().subscribe(result => {
       this.reset();
     });
   }

   onEditCompetence(competence: Competence){
      const dialogRef = this.dialog.open(CompetencePopupComponent, { data: {title: "Редактирование компетенции",
                                                                competence: competence}});

      dialogRef.afterClosed().subscribe(result => {
         this.reset();
      });
   }

   onDeleteCompetence(competence: Competence){
     this.dataService.deleteCompetencies(competence);
     this.reset();
   }

}
