import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Specialization} from '../../services/types';
import {DataService} from '../../services/data.service';

@Component({
  selector: 'app-specialization-popup',
  templateUrl: './specialization-popup.component.html',
  styleUrls: ['./specialization-popup.component.css']
})
export class SpecializationPopupComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<SpecializationPopupComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private dataService: DataService) { }

  ngOnInit() {
  }

  onSave(){
     this.dataService.addSpecialization(this.data.spec);
     this.dialogRef.close();
  }

  onDeleteSpec(){
    this.dataService.deleteSpecialization(this.data.spec);
    this.dialogRef.close();
  }

}
