import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecializationPopupComponent } from './specialization-popup.component';

describe('SpecializationPopupComponent', () => {
  let component: SpecializationPopupComponent;
  let fixture: ComponentFixture<SpecializationPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecializationPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecializationPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
