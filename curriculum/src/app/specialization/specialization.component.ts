import { Component, OnInit, ViewChild } from '@angular/core';
import {MatDialog} from '@angular/material';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {SpecializationPopupComponent} from './specialization-popup/specialization-popup.component';
import {Specialization} from '../services/types';
import {DataService} from '../services/data.service';
import {CompetenciesComponent} from './competencies/competencies.component';
import {ProfilesComponent} from './profiles/profiles.component';

@Component({
  selector: 'app-specialization',
  templateUrl: './specialization.component.html',
  styleUrls: ['./specialization.component.css']
})
export class SpecializationComponent implements OnInit {

  specializations: Specialization[];
  specControl = new FormControl();
  filteredSpecs: Observable<Specialization[]>;
  selectedSpec: Specialization;
  isSelected: boolean = false;

   @ViewChild('competenciesTable')
    private competenciesTable: CompetenciesComponent;

   @ViewChild('educationPrograms')
       private educationPrograms: ProfilesComponent;

  constructor(public dialog: MatDialog,
            private dataService: DataService) { }

  private reset(){
    this.specializations = this.dataService.getSpecializations();
    this.filteredSpecs = this.specControl.valueChanges
      .pipe(
        startWith(''),
         map((value: any) => value && !(value instanceof Specialization) ? this._filter(value) : this.specializations.slice())
      );
    this.selectedSpec = undefined;
  }

  ngOnInit() {
    this.reset();
  }

  displayFn(specialization?: Specialization): string | undefined {
    return specialization ? specialization.code + ' - ' + specialization.name : undefined;
  }

  private _filter(value: string): Specialization[] {
     const filterValue = value.toLowerCase();

     return this.specializations.filter(option => option.code.includes(filterValue) || option.name.toLowerCase().includes(filterValue));
  }

  onSelectSpec(){
    this.isSelected = true;
    if (this.competenciesTable) this.competenciesTable.updateMandatory(this.selectedSpec);
    if (this.educationPrograms) this.educationPrograms.update(this.selectedSpec);
  }

  onAddSpec(){
       const dialogRef = this.dialog.open(SpecializationPopupComponent, { data: {title: "Создание нового направления",
                                                              spec: new Specialization('', ''),
                                                              isExist: false}});

       dialogRef.afterClosed().subscribe(result => {
         this.reset();
       });
     }

     onEditSpec(){
        const dialogRef = this.dialog.open(SpecializationPopupComponent, { data: {title: "Редактирование направления",
                                                                  spec: this.selectedSpec,
                                                                  isExist: true}});

        dialogRef.afterClosed().subscribe(result => {
           this.reset();
        });
     }

}
