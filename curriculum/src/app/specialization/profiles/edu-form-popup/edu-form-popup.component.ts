import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {DataService} from '../../../services/data.service';

@Component({
  selector: 'app-edu-form-popup',
  templateUrl: './edu-form-popup.component.html',
  styleUrls: ['./edu-form-popup.component.css']
})
export class EduFormPopupComponent implements OnInit {

   formsEducation: string[] = ['очная', 'заочная', 'очно-заочная'];

   constructor( public dialogRef: MatDialogRef<EduFormPopupComponent>,
                  @Inject(MAT_DIALOG_DATA) public data: any,
                  private dataService: DataService) { }

  ngOnInit() {
  }

  onDeleteForm(){
    this.dataService.deleteEducationForm(this.data.form);
    this.dialogRef.close();
  }

  onSave(){
    this.dataService.addEducationForm(this.data.form);
    this.dialogRef.close();
  }

}
