import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EduFormPopupComponent } from './edu-form-popup.component';

describe('EduFormPopupComponent', () => {
  let component: EduFormPopupComponent;
  let fixture: ComponentFixture<EduFormPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EduFormPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EduFormPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
