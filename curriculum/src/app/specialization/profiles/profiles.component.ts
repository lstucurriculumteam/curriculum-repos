import { Component, OnInit, ViewChild, Input } from '@angular/core';
import {MatTableDataSource, MatSort, MatDialog} from '@angular/material';
import {ProfilePopupComponent} from './profile-popup/profile-popup.component';
import {EduFormPopupComponent} from './edu-form-popup/edu-form-popup.component';
import {Specialization, Profile, EducationForm} from '../../services/types';
import {DataService} from '../../services/data.service';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.css']
})
export class ProfilesComponent implements OnInit {

  @Input() specialization: Specialization;

  profiles: Profile[];
  specEducationForms: EducationForm[];

  displayedColumns: string[] = ['name', 'department', 'educationForms', 'controls'];
  dataSource;

  constructor(public dialog: MatDialog,
              private dataService: DataService) { }

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
     this.specEducationForms = this.dataService.getEducationForms(this.specialization.id);
     this.profiles = this.dataService.getProfiles(this.specialization.id);
     this.dataSource = new MatTableDataSource(this.profiles);
     this.dataSource.sort = this.sort;
   }

  update(spec: Specialization){
    this.specialization = spec;
    this.specEducationForms = this.dataService.getEducationForms(spec.id);
    this.profiles = this.dataService.getProfiles(spec.id);
    this.dataSource.data = this.profiles;
  }

  resetProfiles(){
     this.profiles = this.dataService.getProfiles(this.specialization.id);
     this.dataSource.data = this.profiles;
  }

  resetEduForms(){
    this.specEducationForms = this.dataService.getEducationForms(this.specialization.id);
  }

  onAddProfile(){
     const dialogRef = this.dialog.open(ProfilePopupComponent, { data: {title: "Создание нового профиля",
                                                              profile: new Profile(this.specialization.id, "", "", []),
                                                              specEducationForms: this.specEducationForms}});

      dialogRef.afterClosed().subscribe(result => {
        this.resetProfiles();
      });
  }

  onEditProfile(profile: Profile){
      const dialogRef = this.dialog.open(ProfilePopupComponent, { data: {title: "Редактирование профиля",
                                                              profile: profile,
                                                              specEducationForms: this.specEducationForms}});

      dialogRef.afterClosed().subscribe(result => {
        this.resetProfiles();
      });
  }

  onDeleteProfile(profile: Profile){
    this.dataService.deleteProfile(profile);
    this.resetProfiles();
  }

  onAddEducationForm(){
    const dialogRef = this.dialog.open(EduFormPopupComponent, { data: {title: "Добавление новой формы обучения",
                                                            form: new EducationForm(this.specialization.id, "", ""),
                                                            isExist: false}});

    dialogRef.afterClosed().subscribe(result => {
      this.resetEduForms();
    });
  }

  onEditEducationForm(form: EducationForm){
    const dialogRef = this.dialog.open(EduFormPopupComponent, { data: {title: "Редактирование формы обучения",
                                                                form: form,
                                                                isExist: true}});

    dialogRef.afterClosed().subscribe(result => {
      this.resetEduForms();
    });
  }
}
