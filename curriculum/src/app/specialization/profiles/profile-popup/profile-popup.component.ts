import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {DataService} from '../../../services/data.service';
import {Department} from '../../../services/types';
import { MatSlideToggleChange } from '@angular/material';

@Component({
  selector: 'app-profile-popup',
  templateUrl: './profile-popup.component.html',
  styleUrls: ['./profile-popup.component.css']
})
export class ProfilePopupComponent implements OnInit {

  departments: Department[] =  this.dataService.getDepartments();
   departmentControl = new FormControl();
   filteredDepartments: Observable<string[]>;

  constructor( public dialogRef: MatDialogRef<ProfilePopupComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private dataService: DataService) { }

   ngOnInit() {
        this.filteredDepartments = this.departmentControl.valueChanges
          .pipe(
            startWith(''),
            map(value => this._filter(value))
          );
      }

    private _filter(value: string): string[] {
      const filterValue = value.toLowerCase();
      const departmentList = this.departments.map(val => val.name);
      return departmentList.filter(option => option.toLowerCase().includes(filterValue));
    }

  isSelected(form: string){
    if (this.data.profile.educationForms.indexOf(form) != -1) return true;
    return false;
  }

  onSelectForm(event: MatSlideToggleChange, form: string){
    if (event.checked){
     this.data.profile.educationForms.push(form);
    }
    else {
      this.data.profile.educationForms.splice( this.data.profile.educationForms.indexOf(form), 1);
    }
  }

  onSave(){
    this.dataService.addProfile(this.data.profile);
    this.dialogRef.close();
  }

}
