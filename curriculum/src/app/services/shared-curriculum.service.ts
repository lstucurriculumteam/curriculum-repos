import { Injectable } from '@angular/core';
import {Specialization, Profile, EducationForm, CurriculumComponent} from './types';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedCurriculumService {

  constructor() { }

  specialization: Specialization;
  profile: Profile;
  eduForm: EducationForm;

  updatedEvent = new Subject();

  components: CurriculumComponent[];

  public setEduProgram(specialization: Specialization, profile: Profile, eduForm: EducationForm){
    this.specialization = specialization;
    this.profile = profile;
    this.eduForm = eduForm;
  }

  public getSpecialization(): Specialization{
    return this.specialization;
  }

  public getProfile(): Profile{
    return this.profile;
  }

  public getEducationForm(): EducationForm{
    return this.eduForm;
  }

  public update(){
    this.updatedEvent.next();
  }

  public setCurriculumComponents(components: CurriculumComponent[]){
      this.components = components;
  }

  public getCurriculumComponents(): CurriculumComponent[]{
      return this.components;
  }

}
