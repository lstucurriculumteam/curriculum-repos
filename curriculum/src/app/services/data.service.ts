import { Injectable } from '@angular/core';
import {UserData, Department, Specialization, Competence, EducationForm, Profile,
        Curriculum, Discipline, Practice, FQW, Exam, CurriculumComponent,
        Week, Year, CheckResult} from './types';

@Injectable({
  providedIn: 'root'
})
export class DataService {

   users: UserData[] = [
      new UserData('Иванов Иван Иванович', 'IvanIvanov@gmail.com', '', [true, false, false, false]),
      new UserData('Петров Петр Петрович', 'PetrPetrov@gmail.com', '', [false, true, false, false]),
      new UserData('Сидорова Мария Ивановна', 'MaryaSidorova@gmail.com', 'Автоматизированные системы управления', [false, false, true, true]),
      new UserData('Superuser', 'superuser@gmail.com', 'Автоматизированные системы управления', [true, true, true, true])]

  departments: Department[] = [new Department('Автоматизированные системы управления'),
                                new Department('Высшая математика'),
                                new Department('Иностранные языки'),
                                new Department('История, теория государства и права и конституционное право'),
                                new Department('Прикладная математика'),
                                new Department('Транспортные средства и техносферная безопасность'),
                                new Department('Физвоспитание'),
                                new Department('Физика и биомедицинская техника'),
                                new Department('Философия'),
                                new Department('Экономика')
                              ];

  specializations: Specialization[] = [ new Specialization('09.03.01', 'Информатика и вычислительная техника'),
                                        new Specialization('01.03.04', 'Прикладная математика'),
                                        new Specialization('09.03.04', 'Программная инженерия')];

  competencies: Competence[] = [new Competence(true, 'УК-1', 'Системное и критическое мышление',
                                  'Способен осуществлять поиск, критический анализ и синтез информации, применять системный подход для решения поставленных задач', 2),
                                new Competence(true, 'УК-2', 'Разработка и реализация проектов ',
                                  'Способен определять круг задач в рамках поставленной цели и выбирать оптимальные способы их решения, исходя из действующих правовых норм, имеющихся ресурсов и ограничений ', 2),
                                new Competence(true, 'УК-3', 'Командная работа и лидерство',
                                  'Способен осуществлять социальное взаимодействие и реализовывать свою роль в команде', 2),
                                new Competence(true, 'УК-4', 'Коммуникация',
                                  'Способен осуществлять деловую коммуникацию в устной и письменной формах на государственном языке Российской Федерации и иностранном(ых) языке(ах)', 2),
                                new Competence(true, 'УК-5', 'Межкультурное взаимодействие',
                                  'Способен воспринимать межкультурное разнообразие общества в социально- историческом, этическом и философском контекстах ', 2),
                                new Competence(true, 'УК-6', 'Самоорганизация и саморазвитие (в том числе здоровьесбережение)',
                                  'Способен управлять своим временем, выстраивать и реализовывать траекторию саморазвития на основе принципов образования в течение всей жизни', 2),
                                new Competence(true, 'УК-7', 'Самоорганизация и саморазвитие (в том числе здоровьесбережение)',
                                  'Способен поддерживать должный уровень физической подготовленности для обеспечения полноценной социальной и профессиональной деятельности', 2),
                                new Competence(true, 'УК-8', 'Безопасность жизнедеятельности',
                                  'Способен создавать и поддерживать безопасные условия жизнедеятельности, в том числе при возникновении чрезвычайных ситуаций', 2),
                                new Competence(true, 'ОПК-1', 'Способен применять естественнонаучные и общеинженерные знания, методы математического анализа и моделирования, теоретического и экспериментального исследования в профессиональной деятельности',
                                  '', 2),
                                new Competence(true, 'ОПК-2', 'Способен использовать современные информационные технологии и программные средства, в том числе отечественного производства, при решении задач профессиональной деятельности',
                                  '', 2),
                                new Competence(true, 'ОПК-3', 'Способен решать стандартные задачи ОПК-3.1. 18 профессиональной деятельности на основе информационной и библиографической культуры с применением информационно-коммуникационных технологий и с учетом основных требований информационной безопасности',
                                  '', 2),
                                new Competence(true, 'ОПК-4', 'Способен участвовать в разработке стандартов, норм и правил, а также технической документации, связанной с профессиональной деятельностью',
                                  '', 2),
                                new Competence(true, 'ОПК-5', 'Способен инсталлировать программное и аппаратное обеспечение для информационных и автоматизированных систем',
                                  '', 2),
                                new Competence(true, 'ОПК-6', 'Способен разрабатывать алгоритмы и программы, пригодные для практического использования, применять основы информатики и программирования к проектированию, конструированию и тестированию программных продуктов',
                                  '', 2),
                                new Competence(true, 'ОПК-7', 'Способен применять в практической деятельности основные концепции, принципы, теории и факты, связанные с информатикой',
                                  '', 2),
                                new Competence(true, 'ОПК-8', 'Способен осуществлять поиск, хранение, обработку и анализ информации из различных источников и баз данных, представлять ее в требуемом формате с использованием информационных, компьютерных и сетевых технологий',
                                  '', 2),
                                new Competence(false, 'ПК-1', 'Владение классическими концепциями и моделями менеджмента в управлении проектами', '', 2, 0, 0),
                                new Competence(false, 'ПК-2', 'Владение методами контроля проекта и готовностью осуществлять контроль версий', '', 2, 0, 0),
                                new Competence(false, 'ПК-3', 'Способность оформления методических материалов и пособий по применению программных систем', '', 2, 0, 0),
                                new Competence(false, 'ПК-4', 'Готовность к использованию методов и инструментальных средств исследования объектов профессиональной деятельности', '', 2, 0, 0),
                                new Competence(false, 'ПК-5', 'Способность готовить презентации, оформлять научно- технические отчеты по результатам выполненной работы, публиковать результаты исследований в виде статей и докладов на научно-технических конференциях', '', 2, 0, 0),
                                new Competence(false, 'ПК-6', 'Владение навыками моделирования, анализа и использования формальных методов конструирования программного обеспечения', '', 2, 0, 0),
                                new Competence(false, 'ПК-7', 'Способность оценивать временную и емкостную сложность программного обеспечения', '', 2, 0, 0),
                                new Competence(false, 'ПК-8', 'Способность создавать программные интерфейсы', '', 2, 0, 0),
                                new Competence(false, 'ПК-9', 'Владение навыками использования операционных систем, сетевых технологий, средств разработки программного интерфейса, применения языков и методов формальных спецификаций, систем управления базами данных', '', 2, 0, 0),
                                new Competence(false, 'ПК-10', 'Владение навыками использования различных технологий разработки программного обеспечения', '', 2, 0, 0),
                                new Competence(false, 'ПК-11', 'Владение концепциями и атрибутами качества программного обеспечения (надежности, безопасности, удобства использования), в том числе роли людей, процессов, методов, инструментов и технологий обеспечения качества', '', 2, 0, 0),
                                new Competence(false, 'ПК-12', 'Владение стандартами и моделями жизненного цикла', '', 2, 0, 0),];

  educationForms: EducationForm[] = [new EducationForm(2, 'очная', '4 года'),
                                            new EducationForm(2, 'очно-заочная', '5 лет'),
                                            new EducationForm(2, 'заочная', '5 лет')];

  profiles: Profile[] = [new Profile(2, 'Разработка программно-информационных систем', 'Автоматизированных систем управления', ['очная'])];

  curriculumes: Curriculum[] = [new Curriculum('РУП1', 1, 2, 0, 0), new Curriculum('РУП2', 2, 2, 0, 0)];

  disciplines: Discipline[] = [new Discipline('Программирование', 'Автоматизированные системы управления'),
                                new Discipline('Объектно-ориентированное программирование', 'Автоматизированные системы управления'),
                                new Discipline('История', 'История, теория государства и права и конституционное право'),
                                new Discipline('Иностранный язык', 'Иностранне яззыки'),
                                new Discipline('Математический анализ', 'Высшая математика'),
                                new Discipline('Аналитическая геометрия', 'Высшая математика'),
                                new Discipline('Философия', 'Философия'),
                                new Discipline('Экономика', 'Экономика'),
                                new Discipline('Линейная алгебра и функции нескольких переменных', 'Высшая математика'),
                                new Discipline('Физика', 'Физика и биомедицинская техника'),
                                new Discipline('Дискретная математика', 'Прикладная математика'),
                                new Discipline('Теория вероятностей', 'Высшая математика'),
                                new Discipline('Операционные системы', 'Автоматизированные системы управления'),
                                new Discipline('Базы данных', 'Автоматизированные системы управления'),
                                new Discipline('Архитектура ЭВМ', 'Автоматизированные системы управления'),
                                new Discipline('Компьютерные сети', 'Автоматизированные системы управления'),
                                new Discipline('Проектирование программного обеспечения', 'Автоматизированные системы управления'),
                                new Discipline('Тестирование и отладка программного обеспечения', 'Автоматизированные системы управления'),
                                new Discipline('Безопасность жизнедеятельности', 'Транспортные средства и техносферная безопасность'),
                                new Discipline('Физическая культура', 'Физвоспитание'),
                                new Discipline('Информатика', 'Автоматизированные системы управления'),
                                new Discipline('Правоведение', 'История, теория государства и права и конституционное право'),
                                new Discipline('Типы и структуры данных', 'Автоматизированные системы управления'),
                                new Discipline('Логика и теория алгоритмов', 'Автоматизированные системы управления'),
                                new Discipline('Вычислительные алгоритмы', 'Автоматизированные системы управления'),
                                new Discipline('Основы электроники', 'Автоматизированные системы управления'),
                                new Discipline('Компьютерная графика', 'Автоматизированные системы управления'),
                                new Discipline('Защита информации', 'Автоматизированные системы управления'),
                                new Discipline('Компьютерное моделирование', 'Автоматизированные системы управления'),
                                new Discipline('Функционально-логическое программирование', 'Автоматизированные системы управления'),
                                new Discipline('Математическая статистика', 'Прикладная математика')
                              ];

  curriculumComponents: CurriculumComponent[] = [new CurriculumComponent(0, new Discipline('Логика и теория алгоритмов', 'Автоматизированные системы управления'), 1, 0),
                                                  new CurriculumComponent(0, new Discipline('Защита информации', 'Автоматизированные системы управления'), 1, 0),
                                                  new CurriculumComponent(0, new Discipline('Моделирование', 'Автоматизированные системы управления'), 1, 0),
                                                  new CurriculumComponent(0, new Discipline('Математическая статистика', 'Прикладная математика'), 1, 0),
                                                  new CurriculumComponent(0, new Discipline('Программирование', 'Автоматизированные системы управления'), 1, 9),
                                                  new CurriculumComponent(0, new Discipline('Программирование', 'Автоматизированные системы управления'), 1, 22),
                                                  new CurriculumComponent(0, new Discipline('Объектно-ориентированное программирование', 'Автоматизированные системы управления'), 1, 25),
                                                  new CurriculumComponent(0, new Discipline('Правоведение', 'История, теория государства и права и конституционное право'), 1, 1),
                                                  new CurriculumComponent(0, new Discipline('История', 'История, теория государства и права и конституционное право'), 1, 4),
                                                  new CurriculumComponent(0, new Discipline('Безопасность жизнедеятельности', 'Транспортные средства и техносферная безопасность'), 1, 1),
                                                  new CurriculumComponent(0, new Discipline('Иностранный язык', 'Иностранные языки'), 1, 2),
                                                  new CurriculumComponent(0, new Discipline('Иностранный язык', 'Иностранные языки'), 1, 2),
                                                  new CurriculumComponent(0, new Discipline('Иностранный язык', 'Иностранные языки'), 1, 3),
                                                  new CurriculumComponent(0, new Discipline('Иностранный язык', 'Иностранные языки'), 1, 3),
                                                  new CurriculumComponent(0, new Discipline('Философия', 'Философия'), 1, 4),
                                                  new CurriculumComponent(0, new Discipline('Экономика', 'Экономика'), 1, 16),
                                                  new CurriculumComponent(0, new Discipline('Физкультура', 'Физвоспитание'), 1, 5),
                                                  new CurriculumComponent(0, new Discipline('Физкультура', 'Физвоспитание'), 1, 5),
                                                  new CurriculumComponent(0, new Discipline('Физкультура', 'Физвоспитание'), 1, 6),
                                                  new CurriculumComponent(0, new Discipline('Физкультура', 'Физвоспитание'), 1, 6),
                                                  new CurriculumComponent(0, new Discipline('Математический анализ', 'Высшая математика'), 1, 8),
                                                  new CurriculumComponent(0, new Discipline('Математический анализ', 'Высшая математика'), 1, 8),
                                                  new CurriculumComponent(0, new Discipline('Аналитическая геометрия', 'Высшая математика'), 1, 8),
                                                  new CurriculumComponent(0, new Discipline('Линейная алгебра и функции нескольких переменных ', 'Высшая математика'), 1, 8),
                                                  new CurriculumComponent(0, new Discipline('Теория вероятности', 'Высшая математика'), 1, 8),
                                                  new CurriculumComponent(0, new Discipline('Дискретная математика', 'Прикладная математика'), 1, 8),
                                                  new CurriculumComponent(0, new Discipline('Физика', 'Физика и биомедицинская техника'), 1, 8),
                                                  new CurriculumComponent(0, new Discipline('Физика', 'Физика и биомедицинская техника'), 1, 8),
                                                  new CurriculumComponent(0, new Discipline('Информатика', 'Автоматизированные системы управления'), 1, 8),
                                                  new CurriculumComponent(0, new Discipline('Проектирование программного обеспечения', 'Автоматизированные системы управления'), 1, 17),
                                                  new CurriculumComponent(0, new Discipline('Инженерная графика', 'Автоматизированные системы управления'), 1, 11),
                                                  new CurriculumComponent(0, new Discipline('Вычислительные алгоритмы', 'Автоматизированные системы управления'), 1, 19),
                                                  new CurriculumComponent(0, new Discipline('Тестирование и отладка программного обеспечения', 'Автоматизированные системы управления'), 1, 13),
                                                  new CurriculumComponent(0, new Discipline('Базы данных', 'Автоматизированные системы управления'), 1, 15),
                                                  new CurriculumComponent(0, new Discipline('Архитектура ЭВМ', 'Автоматизированные системы управления'), 1, 24),
                                                  new CurriculumComponent(0, new Discipline('Типы и структуры данных', 'Автоматизированные системы управления'), 1, 24),
                                                  new CurriculumComponent(0, new Discipline('Компьютерные сети', 'Автоматизированные системы управления'), 1, 25),
                                                  new CurriculumComponent(0, new Discipline('Функционально-логическое программирование', 'Автоматизированные системы управления'), 1, 25),
                                                  new CurriculumComponent(0, new Practice('Учебная практика', 'Ознакомительная', 'Автоматизированные системы управления'), 2, 25),
                                                  new CurriculumComponent(0, new Practice('Учебная практика', 'Проектно-технологическая', 'Автоматизированные системы управления'), 2, 26),
                                                  new CurriculumComponent(0, new Practice('Учебная практика', 'Научно-исследовательская работа', 'Автоматизированные системы управления'), 2, 8),
                                                  new CurriculumComponent(0, new Practice('Учебная практика', 'Научно-исследовательская работа', 'Автоматизированные системы управления'), 2, 8),
                                                  new CurriculumComponent(0, new Practice('Учебная практика', 'Научно-исследовательская работа', 'Автоматизированные системы управления'), 2, 8),
                                                  new CurriculumComponent(0, new Practice('Учебная практика', 'Научно-исследовательская работа', 'Автоматизированные системы управления'), 2, 8),
                                                  new CurriculumComponent(0, new Practice('Производственная практика', 'Эксплуатационная', 'Автоматизированные системы управления'), 2, 27),
                                                  new CurriculumComponent(0, new Practice('Производственная практика', 'Эксплуатационная', 'Автоматизированные системы управления'), 2, 27),
                                                  new CurriculumComponent(0, new FQW('Автоматизированные системы управления'), 3, 20)]

  years: Year[] = [];

  constructor() { }

  public getDepartments(): Department[]{
    return this.departments;
  }

  public getUserRights(login: string, password: string): number[]{
    const index = this.users.findIndex(val => val.email == login && val.password == password);
    if (index != -1){
      return this.users[index].rights.map((val, index) => {
                                          if (val) return ++index;
                                          else return 0;
                                        })
                                        .filter(val => val != 0);
    }
    return undefined;
  }

  public getUsers(): UserData[]{
    return this.users;
  }

  public addUser(user: UserData){
    this.addItem(this.users, user);
  }

  public deleteUser(user: UserData){
    this.deleteItem(this.users, user);
  }

  public getSpecializations(): Specialization[]{
    return this.specializations;
  }

  public addSpecialization(specialization: Specialization){
    this.addItem(this.specializations, specialization);
  }

  public deleteSpecialization(specialization: Specialization){
    this.deleteItem(this.specializations, specialization);
  }

  public getMandatoryCompetencies(specId: number): Competence[]{
    return this.competencies.filter(value => value.mandatory && value.specializationId === specId);
  }

  public addCompetencies(competence: Competence){
    this.addItem(this.competencies, competence);
  }

  public deleteCompetencies(competence: Competence){
    this.deleteItem(this.competencies, competence);
  }

  public getEducationForms(specId: number): EducationForm[]{
     return this.educationForms.filter(value => value.specializationId === specId);
  }

  public addEducationForm(eduForm: EducationForm){
     this.addItem(this.educationForms, eduForm);
  }

  public deleteEducationForm(eduForm: EducationForm){
     this.deleteItem(this.educationForms, eduForm);
  }

  public getProfiles(specId: number): Profile[]{
      return this.profiles.filter(value => value.specializationId === specId);
  }

  public addProfile(profile: Profile){
     this.addItem(this.profiles, profile);
  }

  public deleteProfile(profile: Profile){
     this.deleteItem(this.profiles, profile);
  }

  public getCompetencies(specId: number, profileId: number, eduFormId: number){
    return this.competencies.filter(value => !value.mandatory && value.specializationId === specId
                  && value.profileId === profileId && value.eduFormId === eduFormId);
  }

  public getCurriculumes(specId: number, profileId: number, eduFormId: number): Curriculum[]{
    return this.curriculumes.filter(value => value.specializationId === specId
                             && value.profileId === profileId && value.eduFormId === eduFormId);
  }

  public addCurriculum(curriculum: Curriculum){
    this.addItem(this.curriculumes, curriculum);
  }

  public deleteCurriculum(curriculum: Curriculum){
    this.deleteItem(this.curriculumes, curriculum);
  }

  public getCurriculumComponents(curriculumId: number): CurriculumComponent[]{
    return this.curriculumComponents.filter(value => value.curriculumId == curriculumId);
  }

   public addCurriculumComponent(component: CurriculumComponent){
     this.addItem(this.curriculumComponents, component);
   }

   public deleteCurriculumComponent(component: CurriculumComponent){
     this.deleteItem(this.curriculumComponents, component);
   }

   public getDisciplines(department: string): Discipline[]{
     return this.disciplines.filter(value => value.department == department);
   }

   public getYears(curriculumId: number): Year[]{
    return this.years.filter(value => value.curriculumId == curriculumId);
   }

   public addYear(yearNumber: number, curriculumId: number){
     const weeks: Week[] = [];
     for (let i = 0; i < 52; ++i){
      weeks.push(new Week(i+1, 1));
     }
     this.years.push(new Year(curriculumId, yearNumber, weeks));
   }

   public deleteYear(yearNumber: number, curriculumId: number){
      const index = this.years.findIndex(value => value.num == yearNumber && value.curriculumId == curriculumId);
      this.years.splice(index, 1);
   }

   public changeWeek(yearNumber: number, weekNumber: number, type:number, curriculumId: number){
      const index = this.years.findIndex(value => value.num == yearNumber && value.curriculumId == curriculumId);
      this.years[index].weeks.filter(value => value.num == weekNumber).forEach(value => value.type = type);
   }

   public editComponent(component: CurriculumComponent){
      this.addItem(this.curriculumComponents, component);
   }

   public checkCurriculum(curriculumId: number): CheckResult{
      const result = new CheckResult(true);
      const components = this.curriculumComponents.filter(value => value.curriculumId == curriculumId);
      const discipline = components.filter(value => value.type == 1)
                             .reduce((accumulator, currentValue) => accumulator + currentValue.content.practiceWorks +
                                            currentValue.content.classroom + currentValue.content.consultation + currentValue.content.independentWork, 0);
      if (discipline < 160) result.discipline = true;
      const practice = components.filter(value => value.type == 2)
                              .reduce((accumulator, currentValue) => accumulator + currentValue.content.practiceWorks +
                                             currentValue.content.classroom + currentValue.content.consultation + currentValue.content.independentWork, 0);
      if (practice < 20) result.practice = true;
      const sfs = components.filter(value => value.type == 3)
                              .reduce((accumulator, currentValue) => accumulator + currentValue.content.practiceWorks +
                                             currentValue.content.classroom + currentValue.content.consultation + currentValue.content.independentWork, 0);
      if (sfs < 9) result.sfs = true;
      const total = components.reduce((accumulator, currentValue) => accumulator + currentValue.content.practiceWorks +
                        currentValue.content.classroom + currentValue.content.consultation + currentValue.content.independentWork, 0);
      if (total != 240) result.total = true;
      for (let i = 1; i<=4; ++i){
        const year = components.filter(value => value.content.course == i)
                            .reduce((accumulator, currentValue) => accumulator + currentValue.content.practiceWorks +
                                           currentValue.content.classroom + currentValue.content.consultation + currentValue.content.independentWork, 0);
        if (year > 70) {
          result.year = true;
          break;
        }
      }
      const mandatory = components.filter(value => value.content.component == 1)
                                  .reduce((accumulator, currentValue) => accumulator + currentValue.content.practiceWorks +
                                                 currentValue.content.classroom + currentValue.content.consultation + currentValue.content.independentWork, 0);
      if (mandatory < 96) result.mandatory = true;
      return result;
   }

  private addItem(target: any[], item: any){
    const index = target.findIndex(val => val.id == item.id);
    if (index == -1){
      target.push(item);
    }
    else {
      target.splice(index, 1, item);
    }
  }

  private deleteItem(target: any[], item: any){
    const index = target.indexOf(item);
    target.splice( index, 1);
  }

}
