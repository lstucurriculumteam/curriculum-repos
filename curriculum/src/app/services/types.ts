export class UserData {
  id: number;
  name: string;
  email: string;
  department: string;
  rights: boolean[];
  password: string;

  constructor(name: string, email: string, department: string, rights: boolean[]){
    this.id = UserData.getId();
    this.name = name;
    this.email = email;
    this.department = department;
    this.rights = rights;
    this.password = '111';
  }

  private static _id: number = 0;

  private static getId(){
    return this._id++;
  }
}

export class Department {
  id: number;
  name: string;

  constructor(name: string){
     this.id = Department.getId();
     this.name = name;
  }

  private static _id: number = 0;

  private static getId(){
    return this._id++;
  }
}

export class Specialization {
  id: number;
  code: string;
  name: string;

  constructor(code: string, name: string){
    this.id = Specialization.getId();
    this.code = code;
    this.name = name;
  }

  private static _id: number = 0;

  private static getId(){
    return this._id++;
  }
}

export class Competence {
  id: number;
  mandatory: boolean;
  specializationId: number;
  profileId: number;
  eduFormId: number;
  code: string;
  name: string;
  description: string;

  constructor(mandatory: boolean, code: string, name: string, description: string,
              specializationId: number, profileId?: number, eduFormId?: number){
    this.id = Competence.getId();
    this.mandatory = mandatory;
    this.code = code;
    this.name = name;
    this.description = description;
    this.specializationId = specializationId;
    if (profileId !== undefined) this.profileId = profileId;
    if (eduFormId !== undefined) this.eduFormId = eduFormId;
  }

  private static _id: number = 0;

  private static getId(){
    return this._id++;
  }
}

export class EducationForm{
  id: number;
  specializationId: number;
  name: string;
  period: string;

  constructor(specializationId: number, name: string, period: string){
    this.id = EducationForm.getId();
    this.specializationId = specializationId;
    this.name = name;
    this.period = period;
  }

  private static _id: number = 0;

  private static getId(){
    return this._id++;
  }
}

export class Profile{
  id: number;
  specializationId: number;
  name: string;
  department: string;
  educationForms: string[];

  constructor(specializationId: number, name: string, department: string, educationForms: string[]){
    this.id = Profile.getId();
    this.specializationId = specializationId;
    this.name = name;
    this.department = department;
    this.educationForms = educationForms;
  }

  private static _id: number = 0;

  private static getId(){
    return this._id++;
  }
}

export class Curriculum{
  id: number;
  name: string;
  status: number;
  specializationId: number;
  profileId: number;
  eduFormId: number;

  constructor(name: string, status: number, specializationId: number, profileId: number, eduFormId: number){
    this.id = Curriculum.getId();
    this.name = name;
    this.status = status;
    this.specializationId = specializationId;
    this.profileId = profileId;
    this.eduFormId = eduFormId;
  }

  private static _id: number = 0;

  private static getId(){
    return this._id++;
  }
}


export class Discipline{
  id: number;
  name: string;
  department: string;

  constructor(name: string, department: string) {
    this.id = Discipline.getId();
    this.name = name;
    this.department = department;
  }

  private static _id: number = 0;

  private static getId(){
    return this._id++;
  }
}

export class Practice{
  id: number;
  kind: string;
  type: string;
  department: string;

  constructor(kind: string, type: string, department: string) {
    this.id = Practice.getId();
    this.kind = kind;
    this.type = type;
    this.department = department;
  }

  private static _id: number = 0;

  private static getId(){
    return this._id++;
  }
}

export class FQW{
  id: number;
  department: string;

  constructor(department: string) {
    this.id = FQW.getId();
    this.department = department;
  }

  private static _id: number = 0;

  private static getId(){
    return this._id++;
  }
}

export class Exam{
  id: number;
  discipline: Discipline;

  constructor(discipline: Discipline){
    this.id = Exam.getId();
    this.discipline = discipline;
  }

  private static _id: number = 0;

  private static getId(){
    return this._id++;
  }
}

type ComponentType =  Discipline | Practice | FQW | Exam;

export class CurriculumComponent{
  id: number;
  curriculumId: number;
  value: ComponentType;
  type: number;
  competenceId: number;
  content: ComponentContent;

  constructor(curriculumId: number, value: ComponentType, type: number, competenceId: number) {
    this.id = CurriculumComponent.getId();
    this.curriculumId = curriculumId;
    this.value = value;
    this.type = type;
    this.competenceId = competenceId;
    this.content = new ComponentContent(1,1,type);
  }

  setContent( content: ComponentContent){
    this.content = content;
  }

  private static _id: number = 0;

  private static getId(){
    return this._id++;
  }
}

export class ComponentContent{
  id: number;
  course: number;
  term: number;
  unit: number;
  classNum: number;
  kind: number;
  component: number;
  lectures: number;
  labWorks: number;
  practiceWorks: number;
  classroom: number;
  consultation: number;
  independentWork: number;
  intermediateControl: number;
  certifications: boolean[];

  constructor(course: number, term: number, type: number) {
      this.id = ComponentContent.getId();
      this.course = course;
      this.term = term;
      if (type == 1){
        this.unit = 1;
        this.classNum = 11;
      }
      else if (type == 2){
         this.unit = 2;
         this.classNum = 6;
      }
      else {
        this.unit = 3;
        this.classNum = 10;
      }
      this.kind = 1;
      this.component = 6;
      this.lectures = 0;
      this.labWorks = 0;
      this.practiceWorks = 0;
      this.classroom = 0;
      this.consultation = 0;
      this.independentWork = 0;
      this.intermediateControl = 0;
      this.certifications = [false, false, false, false, false, false];
  }

  private static _id: number = 0;

  private static getId(){
    return this._id++;
  }
}

export class Week{
  id: number;
  num: number;
  type: number;

  constructor(num: number, type: number){
    this.id = Week.getId();
    this.num = num;
    this.type = type;
  }

  private static _id: number = 0;

  private static getId(){
    return this._id++;
  }
}

export class Year{
  id: number;
  curriculumId: number;
  num: number;
  weeks: Week[]

  constructor(curriculumId: number, num: number, weeks: Week[]){
    this.id = Year.getId();
    this.curriculumId = curriculumId;
    this.num = num;
    this.weeks = weeks;
  }

  private static _id: number = 0;

  private static getId(){
    return this._id++;
  }
}

export class CheckResult{
  discipline: boolean;
  practice: boolean;
  sfs: boolean;
  total: boolean;
  year: boolean;
  mandatory: boolean;

  constructor(flag: boolean){
    this.discipline = false;
    this.practice = false;
    this.sfs = false;
    this.total = false;
    this.year = false;
    this.mandatory = false;
  }
}
