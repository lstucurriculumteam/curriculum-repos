import { TestBed, inject } from '@angular/core/testing';

import { SharedCurriculumService } from './shared-curriculum.service';

describe('SharedCurriculumService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SharedCurriculumService]
    });
  });

  it('should be created', inject([SharedCurriculumService], (service: SharedCurriculumService) => {
    expect(service).toBeTruthy();
  }));
});
