import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {UserConfig} from '../auth/user-config';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {

  constructor(private router: Router,
              private userConfig: UserConfig) { }

  ngOnInit() { }

  onLogOut() {
      var date = new Date(0);
      document.cookie = 'login=; expires=' + date.toUTCString();
      document.cookie= 'rights=; expires=' + date.toUTCString();
      this.router.navigate(['login']);
  }

}
