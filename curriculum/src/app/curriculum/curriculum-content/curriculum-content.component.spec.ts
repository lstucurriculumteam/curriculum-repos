import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurriculumContentComponent } from './curriculum-content.component';

describe('CurriculumContentComponent', () => {
  let component: CurriculumContentComponent;
  let fixture: ComponentFixture<CurriculumContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurriculumContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurriculumContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
