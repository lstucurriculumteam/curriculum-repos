import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material';
import {CheckPopupComponent} from './check-popup/check-popup.component';
import { ActivatedRoute } from '@angular/router';
import {UserConfig} from '../../auth/user-config';
import {DataService} from '../../services/data.service';

@Component({
  selector: 'app-curriculum-content',
  templateUrl: './curriculum-content.component.html',
  styleUrls: ['./curriculum-content.component.css']
})
export class CurriculumContentComponent implements OnInit {

  curriculumId: number;

  constructor(public dialog: MatDialog,
              private route: ActivatedRoute,
              private userConfig: UserConfig,
              private dataService: DataService) { }

  ngOnInit() {
    this.curriculumId = Number(this.route.snapshot.paramMap.get('id'));
  }

  onCheck(){
    const checkResult = this.dataService.checkCurriculum(this.curriculumId);
    const dialogRef = this.dialog.open(CheckPopupComponent, { data: { disciplineError: checkResult.discipline,
                                                                      practiceError: checkResult.practice,
                                                                      sfsError: checkResult.sfs,
                                                                      totalError: checkResult.total,
                                                                      yearError: checkResult.year,
                                                                      mandatoryPartError: checkResult.mandatory}});

  }

}
