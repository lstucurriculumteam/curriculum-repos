import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckPopupComponent } from './check-popup.component';

describe('CheckPopupComponent', () => {
  let component: CheckPopupComponent;
  let fixture: ComponentFixture<CheckPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
