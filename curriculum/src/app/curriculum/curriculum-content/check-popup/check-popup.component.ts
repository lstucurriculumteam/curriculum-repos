import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-check-popup',
  templateUrl: './check-popup.component.html',
  styleUrls: ['./check-popup.component.css']
})
export class CheckPopupComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<CheckPopupComponent>,
                    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() { }

}
