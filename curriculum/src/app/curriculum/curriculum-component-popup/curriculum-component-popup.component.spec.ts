import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurriculumComponentPopupComponent } from './curriculum-component-popup.component';

describe('CurriculumComponentPopupComponent', () => {
  let component: CurriculumComponentPopupComponent;
  let fixture: ComponentFixture<CurriculumComponentPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurriculumComponentPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurriculumComponentPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
