import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {DataService} from '../../services/data.service';

@Component({
  selector: 'app-curriculum-component-popup',
  templateUrl: './curriculum-component-popup.component.html',
  styleUrls: ['./curriculum-component-popup.component.css']
})
export class CurriculumComponentPopupComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<CurriculumComponentPopupComponent>,
                  @Inject(MAT_DIALOG_DATA) public data: any,
                  private dataService: DataService) { }

  ngOnInit() {
  }

  getTotalLaborIntensity(): number{
    return Number(this.data.component.content.classroom) + Number(this.data.component.content.consultation) +
            Number(this.data.component.content.independentWork) + Number(this.data.component.content.intermediateControl);
  }

  onSave(){
    this.dataService.editComponent(this.data.component);
    this.dialogRef.close();
  }

}
