import { Component, OnInit, Input} from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {MatDialog} from '@angular/material';
import {CurriculumComponentPopupComponent} from '../curriculum-component-popup/curriculum-component-popup.component';
import {CurriculumComponent} from '../../services/types';
import {DataService} from '../../services/data.service';
import {SharedCurriculumService} from '../../services/shared-curriculum.service';


@Component({
  selector: 'app-curriculum-table',
  templateUrl: './curriculum-table.component.html',
  styleUrls: ['./curriculum-table.component.css']
})
export class CurriculumTableComponent implements OnInit {

  @Input() curriculumId: number;

  termsLabels = ['1-ый', '2-ой', '3-ий', '4-ый', '5-ый', '6-ой', '7-ой', '8-ой'];

  disciplines: CurriculumComponent[][] = [];

  practices: CurriculumComponent[][] = [];

  sfs: CurriculumComponent[][] = [];

  components: CurriculumComponent[];

  constructor(public dialog: MatDialog,
               private dataService: DataService,
               private sharedService: SharedCurriculumService) { }

  ngOnInit() {
    this.reset();
  }

  reset(){
    this.components = this.dataService.getCurriculumComponents(this.curriculumId);
    for (let i=1; i<=8; ++i){
      let disciplines = [];
      let practices = [];
      let sfs = [];
      this.components.filter(value => value.content.term == i).forEach(value => {
        if (value.type == 1){
          disciplines.push(value);
        }
        else if (value.type == 2){
          practices.push(value);
        }
        else {
          sfs.push(value);
        }
      });
      this.disciplines.push(disciplines);
      this.practices.push(practices);
      this.sfs.push(sfs);
    }
  }

  onEditContent(component: CurriculumComponent){
     const dialogRef = this.dialog.open(CurriculumComponentPopupComponent, { data: {component: component},
                                                                              width: '500px'});

      dialogRef.afterClosed().subscribe(result => {
        this.reset();
      });
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
    const id = (<CurriculumComponent>JSON.parse(JSON.stringify(event.container.data[0]))).id;
    const component = this.components.find(value => value.id == id);
    const index = Number(event.container.id.split('-')[3])%8;
    component.content.term = index + 1;
    component.content.course = Math.ceil((index + 1)/2);
    this.dataService.editComponent(component);
    console.log(component);
    this.reset();
  }

}
