import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurriculumPopupComponent } from './curriculum-popup.component';

describe('CurriculumPopupComponent', () => {
  let component: CurriculumPopupComponent;
  let fixture: ComponentFixture<CurriculumPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurriculumPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurriculumPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
