import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {DataService} from '../../services/data.service';

@Component({
  selector: 'app-curriculum-popup',
  templateUrl: './curriculum-popup.component.html',
  styleUrls: ['./curriculum-popup.component.css']
})
export class CurriculumPopupComponent implements OnInit {

   constructor( public dialogRef: MatDialogRef<CurriculumPopupComponent>,
                 @Inject(MAT_DIALOG_DATA) public data: any,
                 private dataService: DataService) { }

   ngOnInit() {
   }

   onSave(){
     this.dataService.addCurriculum(this.data.curriculum);
     this.dialogRef.close();
   }
}
