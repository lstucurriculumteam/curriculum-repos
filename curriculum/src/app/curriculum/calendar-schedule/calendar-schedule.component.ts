import { Component, OnInit, Input } from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {Week, Year} from '../../services/types';
import {DataService} from '../../services/data.service';

@Component({
  selector: 'app-calendar-schedule',
  templateUrl: './calendar-schedule.component.html',
  styleUrls: ['./calendar-schedule.component.css']
})
export class CalendarScheduleComponent implements OnInit {

  @Input() curriculumId: number;

  calendarSchedule: Year[] = this.dataService.getYears(this.curriculumId);

  displayedColumns: string[] = ['year'];
  weeksColumns: string[] = [];
  dataSource = new MatTableDataSource(this.calendarSchedule);

  constructor(private dataService: DataService) {  }

  ngOnInit() {
    for(var i = 1; i<=52; ++i){
       this.displayedColumns.push(""+i);
       this.weeksColumns.push(""+i);
    }
    for (var i = 1; i <= 4; ++i){
      this.dataService.addYear(i, this.curriculumId);
      this.calendarSchedule = this.dataService.getYears(this.curriculumId);
      this.dataSource.data = this.calendarSchedule;
    }
  }

  reset(){
    this.calendarSchedule = this.dataService.getYears(this.curriculumId);
    this.dataSource.data = this.calendarSchedule;
  }

  getType(type: number): string{
    if (type == 1){
      return 'Т';
    }
    if (type == 2){
      return 'З';
    }
    if (type == 3){
      return 'Э';
    }
    if (type == 4){
      return 'К';
    }
     if (type == 5){
      return 'Г';
    }
    if (type == 6){
      return 'Д';
    }
    if (type == 7){
      return 'А';
    }
    if (type == 8){
      return 'У';
    }
    if (type == 9){
      return 'П';
    }
    if (type == 10){
      return 'Р';
    }
  }

  getTypeColor(type: number): string{
      if (type == 1){
        return 'grey';
      }
      if (type == 2){
        return 'magenta';
      }
      if (type == 3){
        return 'blue';
      }
      if (type == 4){
        return 'orange';
      }
       if (type == 5){
        return 'purple';
      }
      if (type == 6){
        return 'red';
      }
      if (type == 7){
        return 'brown';
      }
      if (type == 8){
        return 'green';
      }
      if (type == 9){
        return 'brown';
      }
      if (type == 10){
        return 'magenta';
      }
    }

  onChangeType(yearNumber: number, weekNumber: number, type: number){
     this.dataService.changeWeek(yearNumber, weekNumber, type, this.curriculumId);
     this.reset();
  }

  onAddYear(){
     this.dataService.addYear(this.calendarSchedule.length+1, this.curriculumId);
     this.reset();
  }

  onDeleteYear(){
     this.dataService.deleteYear(this.calendarSchedule.length, this.curriculumId)
     this.reset();
  }

}
