import { Component, OnInit, ViewChild, Input } from '@angular/core';
import {MatTableDataSource, MatDialog} from '@angular/material';
import {SubjectPopupComponent} from './components-popups/subject-popup/subject-popup.component';
import {PracticePopupComponent} from './components-popups/practice-popup/practice-popup.component';
import {FqwPopupComponent} from './components-popups/fqw-popup/fqw-popup.component';
import {Competence, Specialization, Profile, EducationForm, Discipline, Practice, FQW, Exam, CurriculumComponent} from '../../services/types';
import {DataService} from '../../services/data.service';
import {SharedCurriculumService} from '../../services/shared-curriculum.service';

@Component({
  selector: 'app-matrix',
  templateUrl: './matrix.component.html',
  styleUrls: ['./matrix.component.css']
})
export class MatrixComponent implements OnInit {

  @Input() curriculumId: number;

  specialization: Specialization;
  profile: Profile;
  eduForm: EducationForm;
  competencies: Competence[];
  components: CurriculumComponent[];

  rows: any[];

  displayedColumns: string[] = ['col'];
  dataSource = new MatTableDataSource(this.rows);

   constructor(public dialog: MatDialog,
               private dataService: DataService,
               private sharedService: SharedCurriculumService) { }

   ngOnInit() {
      this.specialization = this.sharedService.getSpecialization();;
      this.profile = this.sharedService.getProfile();;
      this.eduForm = this.sharedService.getEducationForm();
      this.competencies = [];
      this.dataService.getMandatoryCompetencies(this.specialization.id).forEach(val => this.competencies.push(val));
      this.dataService.getCompetencies(this.specialization.id, this.profile.id, this.eduForm.id).forEach(val => this.competencies.push(val));
      this.reset();
   }

   reset(){
      this.components = this.dataService.getCurriculumComponents(this.curriculumId);
      this.rows = [];
      this.competencies.forEach(competence => {
        const componentsList = this.components.filter(component => component.competenceId == competence.id);
        this.rows.push({competence: competence, components: componentsList});
       });
      this.dataSource.data = this.rows;
      this.sharedService.setCurriculumComponents(this.components);
   }

   applyFilter(filterValue: string) {
     this.dataSource.filter = filterValue.trim();
   }

   onAddSubject(competence: Competence){
     const dialogRef = this.dialog.open(SubjectPopupComponent, { data: {title: "Добавление дисциплины",
                                                               component: new CurriculumComponent(this.curriculumId, new Discipline("", ""), 1, competence.id),
                                                               isExist: false}});

     dialogRef.afterClosed().subscribe(result => {
       this.reset();
     });
   }

   onAddPractice(competence: Competence){
      const dialogRef = this.dialog.open(PracticePopupComponent, { data: {title: "Добавление практики",
                                                                 component: new CurriculumComponent(this.curriculumId, new Practice("", "", ""), 2, competence.id),
                                                                 isExist: false}});
      dialogRef.afterClosed().subscribe(result => {
            this.reset();
      });
   }

   onAddFQW(competence: Competence){
      const dialogRef = this.dialog.open(FqwPopupComponent,  { data: {title: "Добавление выпускной квалификационной работы",
                                                               component: new CurriculumComponent(this.curriculumId, new FQW(""), 3, competence.id),
                                                               isExist: true}});

      dialogRef.afterClosed().subscribe(result => {
           this.reset();
      });
   }

   onAddExam(competence: Competence){
      const dialogRef = this.dialog.open(SubjectPopupComponent, { data: {title: "Добавление государственного экзамена",
                                                                 component: new CurriculumComponent(this.curriculumId, new Discipline("", ""), 4, competence.id),
                                                                 isExist: false}});
      dialogRef.afterClosed().subscribe(result => {
            this.reset();
      });
   }

  onEditComponent(component: CurriculumComponent){

    let dialogRef;

    if (component.type == 1){
      dialogRef = this.dialog.open(SubjectPopupComponent, { data: {title: "Редактирование дисциплины",
                                                                      component: component,
                                                                      isExist: true}});
    }
    else if (component.type == 2){
       dialogRef = this.dialog.open(PracticePopupComponent, { data: {title: "Редактирование практики",
                                                                            component: component,
                                                                            isExist: true}});
    }
    else if (component.type == 3){
      dialogRef = this.dialog.open(FqwPopupComponent,  { data: {title: "Редактирование выпускной квалификационной работы",
                                                                        component: component,
                                                                        isExist: true}});
    }
    else{
      dialogRef = this.dialog.open(SubjectPopupComponent, { data: {title: "Редактирование государственного экзамена",
                                                                            component: component,
                                                                            isExist: true}});
    }

    dialogRef.afterClosed().subscribe(result => {
          this.reset();
    });
  }

}
