import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PracticePopupComponent } from './practice-popup.component';

describe('PracticePopupComponent', () => {
  let component: PracticePopupComponent;
  let fixture: ComponentFixture<PracticePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PracticePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PracticePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
