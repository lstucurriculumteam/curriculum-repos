import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {Department, Discipline} from '../../../../services/types';
import {DataService} from '../../../../services/data.service';

@Component({
  selector: 'app-subject-popup',
  templateUrl: './subject-popup.component.html',
  styleUrls: ['./subject-popup.component.css']
})
export class SubjectPopupComponent implements OnInit {

  departments: Department[] =  this.dataService.getDepartments();
  departmentControl = new FormControl();
  filteredDepartments: Observable<string[]>;

  disciplines: Discipline[] = this.dataService.getDisciplines(this.data.component.value.department);;
  disciplineControl = new FormControl();
  filteredDisciplines: Observable<string[]>;

  constructor( public dialogRef: MatDialogRef<SubjectPopupComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private dataService: DataService) { }

  ngOnInit() {
     this.filteredDepartments = this.departmentControl.valueChanges
       .pipe(
         startWith(''),
         map(value => this._filter(value, this.departments))
       );
     this.filteredDisciplines = this.disciplineControl.valueChanges
       .pipe(
         startWith(''),
         map(value => this._filter(value, this.disciplines))
       );
  }

  onSelectDepartment(){
    this.disciplines = this.dataService.getDisciplines(this.data.component.value.department);
    this.filteredDisciplines = this.disciplineControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value, this.disciplines))
      );
  }

  private _filter(value: string, target: any[]): string[] {
      const filterValue = value.toLowerCase();
      const list = target.map(val => val.name);
      return list.filter(option => option.toLowerCase().includes(filterValue));
  }

  onDeleteComponent(){
    this.dataService.deleteCurriculumComponent(this.data.component);
    this.dialogRef.close();
  }

  onSave(){
    this.dataService.addCurriculumComponent(this.data.component);
    this.dialogRef.close();
  }

}
