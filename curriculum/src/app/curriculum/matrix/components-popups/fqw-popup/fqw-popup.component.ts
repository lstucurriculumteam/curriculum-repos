import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {Department} from '../../../../services/types';
import {DataService} from '../../../../services/data.service';

@Component({
  selector: 'app-fqw-popup',
  templateUrl: './fqw-popup.component.html',
  styleUrls: ['./fqw-popup.component.css']
})
export class FqwPopupComponent implements OnInit {

 departments: Department[] =  this.dataService.getDepartments();
 departmentControl = new FormControl();
 filteredDepartments: Observable<string[]>;

 constructor( public dialogRef: MatDialogRef<FqwPopupComponent>,
                 @Inject(MAT_DIALOG_DATA) public data: any,
                  private dataService: DataService) { }

 ngOnInit() {
   this.filteredDepartments = this.departmentControl.valueChanges
     .pipe(
       startWith(''),
       map(value => this._filter(value))
     );
 }

 private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    const departmentsList = this.departments.map(val => val.name);
    return departmentsList.filter(option => option.toLowerCase().includes(filterValue));
 }

 onDeleteComponent(){
   this.dataService.deleteCurriculumComponent(this.data.component);
   this.dialogRef.close();
 }

 onSave(){
   this.dataService.addCurriculumComponent(this.data.component);
   this.dialogRef.close();
 }

}
