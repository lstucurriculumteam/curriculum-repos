import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FqwPopupComponent } from './fqw-popup.component';

describe('FqwPopupComponent', () => {
  let component: FqwPopupComponent;
  let fixture: ComponentFixture<FqwPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FqwPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FqwPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
