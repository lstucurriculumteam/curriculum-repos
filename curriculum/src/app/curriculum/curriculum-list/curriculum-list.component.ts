import { Component, OnInit, ViewChild, Input } from '@angular/core';
import {MatTableDataSource, MatSort, MatDialog} from '@angular/material';
import {CurriculumPopupComponent} from '../curriculum-popup/curriculum-popup.component';
import {Specialization, Profile, EducationForm, Curriculum} from '../../services/types';
import {DataService} from '../../services/data.service';
import {SharedCurriculumService} from '../../services/shared-curriculum.service';

@Component({
  selector: 'app-curriculum-list',
  templateUrl: './curriculum-list.component.html',
  styleUrls: ['./curriculum-list.component.css']
})
export class CurriculumListComponent implements OnInit {

    specialization: Specialization;
    profile: Profile;
    eduForm: EducationForm;

   curriculmes: Curriculum[];

   displayedColumns: string[] = ['name', 'status', 'controls'];
   dataSource = new MatTableDataSource(this.curriculmes);

   constructor(public dialog: MatDialog,
                private dataService: DataService,
                private sharedService: SharedCurriculumService) { }

   @ViewChild(MatSort) sort: MatSort;

   updateSubscription: any;

   ngOnInit() {
      this.reset();
      this.dataSource.sort = this.sort;
      this.updateSubscription = this.sharedService.updatedEvent.subscribe(() => this.reset());
   }

   ngOnDestroy(): void {
      this.updateSubscription.unsubscribe();
   }

  reset(){
      this.specialization = this.sharedService.getSpecialization();;
      this.profile = this.sharedService.getProfile();;
      this.eduForm = this.sharedService.getEducationForm();
      this.curriculmes = this.dataService.getCurriculumes(this.specialization.id, this.profile.id, this.eduForm.id);
      this.dataSource.data = this.curriculmes;
   }

   onAddCurriculum(){
      const dialogRef = this.dialog.open(CurriculumPopupComponent, { data: {title: "Создание РУП",
                                                                curriculum: new Curriculum("", 2, this.specialization.id,
                                                                this.profile.id, this.eduForm.id)}});

      dialogRef.afterClosed().subscribe(result => {
         this.reset();
      });
   }

   onEditCurriculum(curriculum: Curriculum){
      const dialogRef = this.dialog.open(CurriculumPopupComponent, { data: {title: "Создание РУП",
                                                                curriculum: curriculum}});

      dialogRef.afterClosed().subscribe(result => {
         this.reset();
      });
   }

   onDeleteCurriculum(curriculum: Curriculum){
      this.dataService.deleteCurriculum(curriculum);
      this.reset();
   }

   getStatus(status: number): string{
     return status == 1 ? 'основной' : 'черновой';
   }

}
