import { Component, OnInit, ViewChild } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {Specialization, Profile, EducationForm} from '../services/types';
import {DataService} from '../services/data.service';
import {CompetenciesComponent} from '../specialization/competencies/competencies.component';
import {CurriculumListComponent} from './curriculum-list/curriculum-list.component';
import {SharedCurriculumService} from '../services/shared-curriculum.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-curriculum',
  templateUrl: './curriculum.component.html',
  styleUrls: ['./curriculum.component.css'],
  providers: [CurriculumListComponent]
})
export class CurriculumComponent implements OnInit {

  @ViewChild('competenciesTable')
      private competenciesTable: CompetenciesComponent;

   specializations: Specialization[] = this.dataService.getSpecializations();
   specControl = new FormControl();
   filteredSpecs: Observable<Specialization[]>;
   selectedSpec: Specialization;

   profiles: Profile[];
   selectedProfile: Profile;

   educationForms: EducationForm[];
   selectedEducationForm: EducationForm;

   showCompetencies: boolean = true;

   constructor(private dataService: DataService,
               private sharedService: SharedCurriculumService,
               private router: Router) { }

    ngOnInit() {
       this.filteredSpecs = this.specControl.valueChanges
          .pipe(
            startWith(''),
             map((value: any) => value && !(value instanceof Specialization) ? this._filter(value) : this.specializations.slice())
          );
    }

    displayFn(specialization?: Specialization): string | undefined {
      return specialization ? specialization.code + ' - ' + specialization.name : undefined;
    }

    private _filter(value: string): Specialization[] {
       const filterValue = value.toLowerCase();

       return this.specializations.filter(option => option.code.includes(filterValue) || option.name.toLowerCase().includes(filterValue));
    }

    onSelectSpec(){
      this.profiles = this.dataService.getProfiles(this.selectedSpec.id);
      this.selectedProfile = undefined;
      this.selectedEducationForm = undefined;
    }

    onSelectProfile(){
      this.educationForms = this.dataService.getEducationForms(this.selectedSpec.id).filter(value =>
                                                                          this.selectedProfile.educationForms.indexOf(value.name) != -1);
      this.selectedEducationForm = undefined;
    }

    onSelectEduForm(){
      if (this.competenciesTable) this.competenciesTable.update(this.selectedSpec, this.selectedProfile, this.selectedEducationForm);
      this.sharedService.setEduProgram(this.selectedSpec, this.selectedProfile, this.selectedEducationForm);
      this.sharedService.update();
      this.router.navigate(['/curriculum']);
    }
}
