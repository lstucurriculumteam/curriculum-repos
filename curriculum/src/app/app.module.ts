import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSelectModule} from '@angular/material/select';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatTabsModule} from '@angular/material/tabs';
import {MatChipsModule} from '@angular/material/chips';
import {MatListModule} from '@angular/material/list';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDividerModule} from '@angular/material/divider';
import {MatRadioModule} from '@angular/material/radio';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './/app-routing.module';
import { UsersComponent } from './administration/users/users.component';
import { PageComponent } from './page/page.component';
import { UserPopupComponent } from './administration/users/user-popup/user-popup.component';
import { SpecializationComponent } from './specialization/specialization.component';
import { CompetenciesComponent } from './specialization/competencies/competencies.component';
import { CompetencePopupComponent } from './specialization/competencies/competence-popup/competence-popup.component';
import { SpecializationPopupComponent } from './specialization/specialization-popup/specialization-popup.component';
import { ProfilesComponent } from './specialization/profiles/profiles.component';
import { ProfilePopupComponent } from './specialization/profiles/profile-popup/profile-popup.component';
import { EduFormPopupComponent } from './specialization/profiles/edu-form-popup/edu-form-popup.component';
import { CurriculumComponent } from './curriculum/curriculum.component';
import { DisableControlDirective } from './directives/disable-control.directive';
import { MatrixComponent } from './curriculum/matrix/matrix.component';
import { SubjectPopupComponent } from './curriculum/matrix/components-popups/subject-popup/subject-popup.component';
import { FqwPopupComponent } from './curriculum/matrix/components-popups/fqw-popup/fqw-popup.component';
import { PracticePopupComponent } from './curriculum/matrix/components-popups/practice-popup/practice-popup.component';
import { CalendarScheduleComponent } from './curriculum/calendar-schedule/calendar-schedule.component';
import { CurriculumListComponent } from './curriculum/curriculum-list/curriculum-list.component';
import { CurriculumContentComponent } from './curriculum/curriculum-content/curriculum-content.component';
import { CurriculumPopupComponent } from './curriculum/curriculum-popup/curriculum-popup.component';
import { CurriculumTableComponent } from './curriculum/curriculum-table/curriculum-table.component';
import { CurriculumComponentPopupComponent } from './curriculum/curriculum-component-popup/curriculum-component-popup.component';
import { CheckPopupComponent } from './curriculum/curriculum-content/check-popup/check-popup.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UsersComponent,
    PageComponent,
    UserPopupComponent,
    SpecializationComponent,
    CompetenciesComponent,
    CompetencePopupComponent,
    SpecializationPopupComponent,
    ProfilesComponent,
    ProfilePopupComponent,
    EduFormPopupComponent,
    CurriculumComponent,
    DisableControlDirective,
    MatrixComponent,
    SubjectPopupComponent,
    PracticePopupComponent,
    FqwPopupComponent,
    CalendarScheduleComponent,
    CurriculumListComponent,
    CurriculumContentComponent,
    CurriculumPopupComponent,
    CurriculumTableComponent,
    CurriculumComponentPopupComponent,
    CheckPopupComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatMenuModule,
    MatIconModule,
    MatTableModule,
    MatSortModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatTabsModule,
    MatChipsModule,
    MatListModule,
    DragDropModule,
    MatCheckboxModule,
    MatDividerModule,
    MatRadioModule
  ],
  entryComponents: [
     UserPopupComponent,
     SpecializationPopupComponent,
     CompetencePopupComponent,
     ProfilePopupComponent,
     EduFormPopupComponent,
     SubjectPopupComponent,
     PracticePopupComponent,
     FqwPopupComponent,
     CurriculumPopupComponent,
     CurriculumComponentPopupComponent,
     CheckPopupComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
