import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {UserConfig} from '../auth/user-config';
import {UserData} from '../services/types';
import {DataService} from '../services/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  login: string = '';
  password: string = '';
  failed: boolean = false;

  constructor(private router: Router,
              private userConfig: UserConfig,
              private dataService: DataService) { }

  ngOnInit() {
    console.log(document.cookie);
    if (this.getCookie('login')){
      const rights = this.getCookie('rights');
      console.log(rights);
      this.userConfig.addRights(rights.split(',').map(value => Number(value)));
      this.router.navigate(['admin']);
    }
  }

  private getCookie(name) {
    const matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }

  onLogin(){
    const rights = this.dataService.getUserRights(this.login, this.password);
    if (!rights) this.failed = true;
    else {
      const date = new Date(new Date().getTime() + 3600 * 1000);
      document.cookie = 'login=' + this.login + '; expires=' + date.toUTCString();
      document.cookie= 'rights=' + rights + '; expires=' + date.toUTCString();
      this.userConfig.addRights(rights);
      this.router.navigate(['admin']);
    }
  }

}
