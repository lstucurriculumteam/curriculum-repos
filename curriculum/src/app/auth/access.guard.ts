import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import {UserConfig} from './user-config';

@Injectable({
  providedIn: 'root'
})
export class AccessGuard implements CanActivate {

  constructor(private userConfig: UserConfig,
              private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (state.url == '/admin'){
      if (this.userConfig.containsRight(1)){
        return true;
      }
      else if (this.userConfig.containsRight(2)){
        this.router.navigate(['/specialization']);
        return false;
      }
      else if (this.userConfig.containsRight(3) || this.userConfig.containsRight(4)){
        this.router.navigate(['/curriculum']);
        return false;
      }
    }
    if (state.url == '/specialization' && this.userConfig.containsRight(2)){
        return true;
    }
    if (state.url == '/curriculum' && this.userConfig.containsRight(3) || this.userConfig.containsRight(4)){
        return true;
    }
    this.router.navigate(['/login']);
    return false;
  }
}
