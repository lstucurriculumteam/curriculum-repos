import { Injectable } from '@angular/core';
import {UserData} from '../services/types';
import {DataService} from '../services/data.service';

@Injectable({
  providedIn: 'root'
})
export class UserConfig{

  private _user: UserData;
  private _rights: number[] = [];

  constructor(private dataService: DataService){}

  public setUser(login: string){
    const index = this.dataService.getUsers().findIndex(value => value.email == login);
    this._user = this.dataService.getUsers()[index];
  }

  public addRights(rights: number[]){
    this._rights = rights;
  }

  public containsRight(right: number): boolean{
    return this._rights.indexOf(right) == -1 ? false : true;
  }

}
