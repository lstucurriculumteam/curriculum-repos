import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource, MatSort, MatDialog} from '@angular/material';
import {UserData} from '../../services/types';
import {DataService} from '../../services/data.service';
import { UserPopupComponent } from './user-popup/user-popup.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: UserData[] = this.dataService.getUsers();

  displayedColumns: string[] = ['name', 'email', 'department', 'rights', 'controls'];
  dataSource = new MatTableDataSource(this.users);

  constructor(public dialog: MatDialog,
              private dataService: DataService) { }

  @ViewChild(MatSort) sort: MatSort;

   ngOnInit() {
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim();
  }

  onAddUser(){
     const dialogRef = this.dialog.open(UserPopupComponent, { data: {title: "Создание нового пользователя",
                                                              user: new UserData('', '', '', [false, false, false, false])}});

      dialogRef.afterClosed().subscribe(result => {
         this.reset();
      });
  }

  onEditUser(user: UserData){
    const dialogRef = this.dialog.open(UserPopupComponent, { data: {title: "Редактирование пользователя",
                                                             user: user}});

    dialogRef.afterClosed().subscribe(result => {
       this.reset();
    });
  }

  onDeleteUser(user: UserData){
    this.dataService.deleteUser(user);
    this.reset();
  }

  reset(){
    this.users = this.dataService.getUsers();
    this.dataSource.data = this.users;
  }

}
