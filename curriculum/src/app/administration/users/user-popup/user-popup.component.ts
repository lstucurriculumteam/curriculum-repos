import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {Department} from '../../../services/types';
import {DataService} from '../../../services/data.service';

@Component({
  selector: 'app-user-popup',
  templateUrl: './user-popup.component.html',
  styleUrls: ['./user-popup.component.css']
})
export class UserPopupComponent implements OnInit {

  departments: Department[] =  this.dataService.getDepartments();
  departmentControl = new FormControl();
  filteredDepartments: Observable<string[]>;

  constructor( public dialogRef: MatDialogRef<UserPopupComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private dataService: DataService) { }

   ngOnInit() {
      this.filteredDepartments = this.departmentControl.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filter(value))
        );
    }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    const departmentList = this.departments.map(val => val.name);
    return departmentList.filter(option => option.toLowerCase().includes(filterValue));
  }

  onSave(){
    this.dataService.addUser(this.data.user);
    this.dialogRef.close();
  }

}
