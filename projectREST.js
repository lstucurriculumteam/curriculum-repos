var express = require('express');
var crypto = require('crypto');
var oracledb = require('oracledb');
var app = express();
var bodyParser = require('body-parser')
oracledb.outFormat = oracledb.OBJECT; // представление выходных строк как объекты
oracledb.autoCommit = true;// автоматический коммит изменений при обновлении, удалении, вставке
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var conn = { // параметры соединения
  user: "nodejs", // логин
  password: "12345", // пароль
  connectString: "localhost/TEST" // строка соединения: ip-адрес/SID сервиса
};

app.use(accessControlHeaders); // использовать заголовки
app.post('/auth', function (req, res) {
  if (!req.body.password || !req.body.login) { res.json({ msg: 'Login and password required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    //let passHash = crypto.createHash('md5').update(req.body.password).digest('hex');
    let passHash = req.body.password;

    connection.execute(`SELECT * FROM ET_USERS WHERE "LOGIN" = '${req.body.login}'`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      if (result.rows.length == 0) { res.json({ msg: 'Invalid login.' }); return; }
      let OID = result.rows[0].OID;
      let chair = result.rows[0].CHAIR;
      if (result.rows[0].PASSWORD == null)
        connection.execute(`UPDATE ET_USERS SET "PASSWORD" = '${passHash}' WHERE "OID" = '${OID}'`, login);
      else if (result.rows[0].PASSWORD == passHash) login(err, result);
      else { res.json({ msg: 'Invalid login/password.' }); return; }
      function login(err, result) {
        let tokenHash = crypto.createHash('md5').update(OID + "-tokenhash-" + parseInt(new Date().getTime() / 1000)).digest('hex');
        connection.execute(`UPDATE ET_USERS SET "TOKEN" = '${tokenHash}' WHERE "OID" = '${OID}'`, function (err, result) {
          if (err) { console.error(err); res.status(500).json({ error: err }); return; }
          connection.execute(`SELECT r.OID, r.NAME FROM ET_ROLECELLS c INNER JOIN ET_ROLES r ON c.role = r.oid WHERE c."USER" = '${OID}'`, function (err, result) {
            if (err) { console.error(err); res.status(500).json({ error: err }); return; }
            let arr = [];
            result.rows.forEach(e => {
              arr.push(e);
            });
            connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
            res.json({ token: tokenHash, chair: chair, roles: arr });
          });
        });
      }
    });
  });
});

app.get('/*', function (req, res) { res.status(400).send('GET is not allowed!') });

app.use(tokenCheck);

app.post('/calendar/etype', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT * FROM ET_ETYPE`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.json(result.rows);
    });
  });
});
/*app.post('/calendar/get', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.opop) { res.json({ msg: 'OPOP OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT w.OID, WEEK_NUM, TYPE, TYPE_MON, TYPE_TUE, TYPE_WED, TYPE_THU, TYPE_FRI, TYPE_SAT, TYPE_SUN, s.NAME AS SEMESTER
     FROM "Week" w INNER JOIN ET_SEMESTERS s ON w.SEMESTER = s.OID WHERE "OPOP" = '${req.body.opop}'`, function (err, result) {
        if (err) { console.error(err); res.status(500).json({ error: err }); return; }
        connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
        let arr = { "course1": [], "course2": [], "course3": [], "course4": [], "course5": [], "course6": [], "course7": [] };
        result.rows.forEach(e => {
          switch (e.SEMESTER) {
            case "0":
            case "1":
            case "2":
              arr['course1'].push(e);
              break;
            case "3":
            case "4":
              arr['course2'].push(e);
              break;
            case "5":
            case "6":
              arr['course3'].push(e);
              break;
            case "7":
            case "8":
              arr['course4'].push(e);
              break;
            case "9":
            case "10":
              arr['course5'].push(e);
              break;
            case "11":
            case "12":
              arr['course6'].push(e);
              break;
            case "13":
            case "14":
              arr['course7'].push(e);
              break;
          }
        });
        res.json(arr);
      });
  });
});*/
app.post('/calendar/get', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.opop) { res.json({ msg: 'OPOP OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT opop.SEMESTERS, w.OID, WEEK_NUM, TYPE, TYPE_MON, TYPE_TUE, TYPE_WED, TYPE_THU, TYPE_FRI, TYPE_SAT, TYPE_SUN, s.NAME AS SEMESTER
     FROM "Week" w INNER JOIN ET_SEMESTERS s ON w.SEMESTER = s.OID INNER JOIN ET_OPOP opop ON w.OPOP = opop.OID
      WHERE "OPOP" = '${req.body.opop}'`, function (err, result) {
        let semesters = result.rows[0].SEMESTERS;
        if (err) { console.error(err); res.status(500).json({ error: err }); return; }
        connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
        let arr = { "course1": [], "course2": [], "course3": [], "course4": [], "course5": [], "course6": [], "course7": [] };
        result.rows.forEach(e => {
          switch (e.SEMESTER) {
            case "0":
            case "1":
            case "2":
              arr['course1'].push(e);
              break;
            case "3":
            case "4":
              arr['course2'].push(e);
              break;
            case "5":
            case "6":
              arr['course3'].push(e);
              break;
            case "7":
            case "8":
              arr['course4'].push(e);
              break;
            case "9":
            case "10":
              arr['course5'].push(e);
              break;
            case "11":
            case "12":
              arr['course6'].push(e);
              break;
            case "13":
            case "14":
              arr['course7'].push(e);
              break;
          }
        });
        if (semesters < 13)
          delete arr['course7'];
        if (semesters < 11)
          delete arr['course6'];
        if (semesters < 9)
          delete arr['course5'];
        if (semesters < 7)
          delete arr['course4'];
        if (semesters < 5)
          delete arr['course3'];
        if (semesters < 3)
          delete arr['course2'];
        res.json(arr);
      });
  });
});
app.post('/calendar/set', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.opop || !req.body.weeks) { res.json({ msg: 'OPOP OID and week array required.' }); return; }
  let weeksReq = req.body.weeks;
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT OID, NAME FROM ET_SEMESTERS`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      let semesters = {};
      result.rows.forEach(e => {
        semesters[[e.NAME]] = e.OID;
      });
      connection.execute(`SELECT * FROM "Week" WHERE "OPOP" = '${req.body.opop}'`, function (err, result) {
        if (err) { console.error(err); res.status(500).json({ error: err }); return; }
        let weeksBD = result.rows;
        let insertQ = `INSERT ALL`;
        let inserted = false; //Есть ли хотя бы одна вставка?
        let updateQ = ``;
        for (let i = 0; i < weeksReq.length; i++) {
          let exists = false;
          for (let j = 0; j < weeksBD.length; j++) {
            if (weeksReq[i].OID == null) break;
            if (weeksReq[i].OID == weeksBD[j].OID) {
              if (weeksReq[i].WEEK_NUM != weeksBD[j].WEEK_NUM || req.body.opop != weeksBD[j].OPOP || semesters[[weeksReq[i].SEMESTER]] != weeksBD[j].SEMESTER) {
                connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
                res.json({ msg: 'Error in data structure.' });
                return;
              }
              exists = true;
              weeksBD[j].exists = true;
              if (!(weeksReq[i].TYPE == weeksBD[j].TYPE && weeksReq[i].TYPE_MON == weeksBD[j].TYPE_MON && weeksReq[i].TYPE_TUE == weeksBD[j].TYPE_TUE
                && weeksReq[i].TYPE_WED == weeksBD[j].TYPE_WED && weeksReq[i].TYPE_THU == weeksBD[j].TYPE_THU && weeksReq[i].TYPE_FRI == weeksBD[j].TYPE_FRI
                && weeksReq[i].TYPE_SAT == weeksBD[j].TYPE_SAT && weeksReq[i].TYPE_SUN == weeksBD[j].TYPE_SUN))
                updateQ += `UPDATE "Week" SET TYPE = '${weeksReq[i].TYPE}', TYPE_MON = '${weeksReq[i].TYPE_MON}', TYPE_TUE = '${weeksReq[i].TYPE_TUE}', TYPE_WED = '${weeksReq[i].TYPE_WED}', TYPE_THU = '${weeksReq[i].TYPE_THU}', TYPE_FRI = '${weeksReq[i].TYPE_FRI}', TYPE_SAT = '${weeksReq[i].TYPE_SAT}', TYPE_SUN = '${weeksReq[i].TYPE_SUN}' WHERE OID = '${weeksReq[i].OID}';`;
            }
          }
          if (!exists) {
            if (weeksReq[i].OID != null) { res.json({ msg: 'Invalid OID detected.' }); return; }
            insertQ += ` INTO "Week" VALUES (null, '${semesters[[weeksReq[i].SEMESTER]]}', '${req.body.opop}', '${weeksReq[i].WEEK_NUM}', '${weeksReq[i].TYPE}', '${weeksReq[i].TYPE_MON}', '${weeksReq[i].TYPE_TUE}', '${weeksReq[i].TYPE_WED}', '${weeksReq[i].TYPE_THU}', '${weeksReq[i].TYPE_FRI}', '${weeksReq[i].TYPE_SAT}', '${weeksReq[i].TYPE_SUN}') `;
            inserted = true;
          }
        }
        insertQ += ` SELECT * FROM dual;`;
        let deleteQ = '';
        let deleteArr = [];
        let deleted = false;
        for (let j = 0; j < weeksBD.length; j++) {
          if (!weeksBD[j].exists)
            deleteArr.push(weeksBD[j].OID);
        }
        if (deleteArr.length != 0) {
          deleted = true;
          deleteQ = `DELETE FROM "Week" WHERE "OID" IN (`;
          for (let k = 0; k < deleteArr.length; k++) {
            if (k == deleteArr.length - 1)
              deleteQ += `'${deleteArr[k]}');`;
            else
              deleteQ += `'${deleteArr[k]}',`;
          }
        }
        if (inserted)
          updateQ = insertQ + ' ' + updateQ;
        if (deleted)
          updateQ = updateQ + ' ' + deleteQ;
        //ELSE DELETE
        if (updateQ == '') {
          connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
          res.json({ msg: 'No rows affected.' });
          return;
        }
        else {
          updateQ = 'BEGIN ' + updateQ + ' END;';
          connection.execute(updateQ, function (err, result) {
            if (err) { console.error(err); res.status(500).json({ error: err }); return; }
            connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
            res.json({ msg: 'Weeks updated.' });
            return;
          });
          //res.json(updateQ);
        }
      });
    });
  });
});

app.post('/profile', function (req, res) {
  /*let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }*/
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    let chair = req.userinfo.CHAIR;
    let query = `SELECT p.*, c.CODE AS chairCode, f.CODE AS fgosCode FROM "Training_profile" p
    INNER JOIN ET_CHAIRS c ON p.CHAIR = c.OID
    INNER JOIN FGOS f ON p.FGOS = f.OID`;
    if (chair)
      query += ` WHERE "CHAIR" = '${chair}'`;
    connection.execute(query, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.json(result.rows);
    });
  });
});
app.post('/specialities', function (req, res) {
  /*let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }*/
  oracledb.getConnection(conn, function (err, connection) {
    if (!req.body.chair) { res.json({ msg: 'Chair OID required.' }); return; }
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT * FROM ET_SPECIALITIES WHERE "CHAIR" = '${req.body.chair}'`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.json(result.rows);
    });
  });
});
app.post('/eform', function (req, res) {
  /*let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }*/
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT * FROM T_EFORMS`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.json(result.rows);
    });
  });
});
app.post('/fgos', function (req, res) {
  /*let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }*/
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT * FROM FGOS`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.json(result.rows);
    });
  });
});
app.post('/opop', function (req, res) {
  /*let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }*/
  if (!req.body.profile) { res.json({ msg: 'Educational profile OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT op.*, f.VALUE AS eformName FROM ET_OPOP op
    INNER JOIN T_EFORMS f ON op.EFORM = f.OID WHERE "TRAININGPROFILE" = '${req.body.profile}'`, function (err, result) {
        if (err) { console.error(err); res.status(500).json({ error: err }); return; }
        connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
        res.json(result.rows);
      });
  });
});
app.post('/semesters', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.opop) { res.json({ msg: 'OPOP OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT * FROM ET_SEMESTERS WHERE "NAME" BETWEEN 1 AND (SELECT SEMESTERS FROM ET_OPOP WHERE "OID" = '${req.body.opop}')`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.json(result.rows);
    });
  });
});
app.post('/eduplan/dcycle', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.opop) { res.json({ msg: 'OPOP OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT * FROM T_DCYCLE`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.json(result.rows);
    });
  });
});
app.post('/eduplan/distypes', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.opop) { res.json({ msg: 'OPOP OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT * FROM T_DISTYPES`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.json(result.rows);
    });
  });
});
app.post('/eduplan/idztypes', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.opop) { res.json({ msg: 'OPOP OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT * FROM T_IDZTYPES`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.json(result.rows);
    });
  });
});
app.post('/eduplan/ecomponent', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.opop) { res.json({ msg: 'OPOP OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT * FROM T_ECOMPONENT`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.json(result.rows);
    });
  });
});

app.post('/eduplan/list', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.opop) { res.json({ msg: 'OPOP OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT * FROM ET_CURRICULUMS WHERE "OPOP" = '${req.body.opop}' AND rownum <= 100`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.json(result.rows);
    });
  });
});
app.post('/eduplan/insert', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.opop || !req.body.speciality) { res.json({ msg: 'Some info is missing.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`INSERT INTO ET_CURRICULUMS VALUES(null, '${req.body.opop}','${req.body.speciality}',${req.body.code ? `'${req.body.code}'` : `null`},${req.body.name ? `'${req.body.name}'` : `null`},${req.body.flagbase ? `'${req.body.flagbase}'` : `null`},${req.body.year ? `'${req.body.year}'` : `null`},${req.body.active ? `'${req.body.active}'` : `null`},${req.body.oldcode ? `'${req.body.oldcode}'` : `null`},${req.body.subname ? `'${req.body.subname}'` : `null`},${req.body.comments ? `'${req.body.comments}'` : `null`},${req.body.aprotocol ? `'${req.body.aprotocol}'` : `null`},${req.body.adate ? `'${req.body.adate}'` : `null`},${req.body.n_type ? `'${req.body.n_type}'` : `null`},${req.body.g3 ? `'${req.body.g3}'` : `null`},${req.body.flagvupusk ? `'${req.body.flagvupusk}'` : `null`},null,null)`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'Educational plan inserted.' });
    });
  });
});
app.post('/eduplan/fill', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.plan || !req.body.chair) { res.json({ msg: 'Educational plan/chair OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    let matrixDisc = [];
    let planDisc = [];
    connection.execute(`SELECT d.* FROM ET_CURRICULUMS c
    INNER JOIN ET_OPOP opop ON c.OPOP = opop.OID
    INNER JOIN "Training_profile" p ON opop.trainingprofile = p.OID
    INNER JOIN ET_COMPETENCYMATRIX m ON m.trainingprofile = p.OID
    INNER JOIN ET_COMPETENCYCELLS cl ON cl.matrix = m.OID
    INNER JOIN ET_DISCIPLINES d ON cl.DISCIPLINE = d.OID
    WHERE m.MAIN = 1 AND c.OID = '${req.body.plan}'`, function (err, result) {
        if (err) { console.error(err); res.status(500).json({ error: err }); return; }
        if (result.rows.length == 0) { console.error(err); res.json({ msg: 'No main matrix.' }); return; }
        result.rows.forEach(e => {
          matrixDisc.push({ value: e.OID });
        });
        connection.execute(`SELECT OID, DISCIPLINE FROM ET_DSPLANS WHERE "EPLAN" = '${req.body.plan}'`, function (err, result) {
          if (err) { console.error(err); res.status(500).json({ error: err }); return; }
          result.rows.forEach(e => {
            planDisc.push({ value: e.DISCIPLINE, oid: e.OID, exists: false });
          });
          let insertQ = `INSERT ALL`;
          let inserted = false; //Есть ли хотя бы одна вставка?

          for (let i = 0; i < matrixDisc.length; i++) {
            let exists = false;
            for (let j = 0; j < planDisc.length; j++) {
              if (matrixDisc[i].value == planDisc[j].value) {
                exists = true;
                planDisc[j].exists = true;
              }
            }
            if (!exists) {
              insertQ += ` INTO ET_DSPLANS VALUES (null, '${req.body.plan}', '${matrixDisc[i].value}', '${req.body.chair}', '1:1568',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null) `;
              inserted = true;
            }
          }
          insertQ += ` SELECT * FROM dual;`;
          let deleteQ = '';
          let deleteArr = [];
          let deleted = false;
          for (let j = 0; j < planDisc.length; j++) {
            if (!planDisc[j].exists)
              deleteArr.push(planDisc[j].oid);
          }
          if (deleteArr.length != 0) {
            deleted = true;
            deleteQ = `DELETE FROM ET_DSPLANS WHERE "OID" IN (`;
            for (let k = 0; k < deleteArr.length; k++) {
              if (k == deleteArr.length - 1)
                deleteQ += `'${deleteArr[k]}');`;
              else
                deleteQ += `'${deleteArr[k]}',`;
            }
          }
          if (!inserted && !deleted) { res.send({ msg: 'Plan is up to date.'}); return; }
          let query = '';
          if (inserted)
            query = insertQ;
          if (deleted)
            query = query + ' ' + deleteQ;
          query = 'BEGIN ' + query + ' END;';
          connection.execute(query, function (err, result) {
            if (err) { console.error(err); res.status(500).json({ error: err }); return; }
            connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
            res.send({ msg: 'Plan filled from main matrix.' });
          });
          //res.json({ matrix: matrixDisc, plan: planDisc, query: query });
        });
      });
  });
});
app.post('/eduplan/update', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  //if (!req.body.opop || !req.body.plan || !req.body.speciality || !req.body.code || !req.body.name || !req.body.flagbase || !req.body.year || !req.body.active || !req.body.oldcode || !req.body.subname || !req.body.comments || !req.body.aprotocol || !req.body.adate || !req.body.n_type || !req.body.g3 || !req.body.flagvupusk) { res.json({ msg: 'Some info is missing.' }); return; }
  if (!req.body.opop || !req.body.plan || !req.body.speciality) { res.json({ msg: 'Some info is missing.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`UPDATE ET_CURRICULUMS SET "OPOP" =  '${req.body.opop}',"SPECIALITY" = '${req.body.speciality}'${req.body.code ? `,"CODE" = '${req.body.code}'` : ``}${req.body.name ? `, "NAME" = '${req.body.name}'` : ``}${req.body.flagbase ? `, "FLAGBASE" = '${req.body.flagbase}'` : ``}${req.body.year ? `, "YEAR" = '${req.body.year}'` : ``}${req.body.active ? `, "ACTIVE" = '${req.body.active}'` : ``}${req.body.oldcode ? `, "OLDCODE" = '${req.body.oldcode}'` : ``}${req.body.subname ? `, "SUBNAME" = '${req.body.subname}'` : ``}${req.body.comments ? `, "COMMENTS" = '${req.body.comments}'` : ``}${req.body.aprotocol ? `, "APROTOCOL" = '${req.body.aprotocol}'` : ``}${req.body.adate ? `, "ADATE" = '${req.body.adate}'` : ``}${req.body.n_type ? `, "N_TYPE" = '${req.body.n_type}'` : ``}${req.body.g3 ? `, "G3" = '${req.body.g3}'` : ``}${req.body.flagvupusk ? `, "FLAGVUPUSK" = '${req.body.flagvupusk}'` : ``} WHERE "OID" = '${req.body.plan}'`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'Educational plan updated.' });
    });
  });
});
app.post('/eduplan/delete', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.plan) { res.json({ msg: 'Educational plan OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`DELETE FROM ET_CURRICULUMS WHERE "OID" = '${req.body.plan}'`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); res.send({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'Educational plan deleted.' });
    });
  });
});
app.post('/eduplan/get', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.plan) { res.json({ msg: 'Educational plan OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT d.NAME AS disciplineName, c.CODE AS chairName, sem.NAME AS semesterName,t.NAME AS distypeName, cy.NAME AS dcycleName, i.NAME AS idzName,com.NAME AS componentName, s.*
                        FROM ET_DSPLANS s INNER JOIN ET_DISCIPLINES d ON s.DISCIPLINE = d.OID
                        LEFT JOIN ET_CHAIRS c ON s.CHAIR = c.OID
                        LEFT JOIN ET_SEMESTERS sem ON s.SEMESTER = sem.OID
                        LEFT JOIN T_DISTYPES t ON s.DISTYPE = t.OID
                        LEFT JOIN T_DCYCLE cy ON s.DCYCLE = cy.OID
                        LEFT JOIN T_IDZTYPES i ON s.IDZ = i.OID
                        LEFT JOIN T_ECOMPONENT com ON s.ECOMPONENT = s.OID
                        WHERE "EPLAN" = '${req.body.plan}'`, function (err, result) {
        if (err) { console.error(err); res.status(500).json({ error: err }); return; }
        connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
        res.json(result.rows);
      });
  });
});
/*app.post('/eduplan/set', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.plan || !req.body.strings) { res.json({ msg: 'Plan OID/string array required.' }); return; }
  let query = `BEGIN DELETE FROM ET_DSPLANS WHERE "EPLAN" = '${req.body.plan}'; INSERT ALL `;
  req.body.strings.forEach(e => {
    query += `INTO ET_DSPLANS VALUES (null,'${req.body.plan}','${e.DISCIPLINE}','${e.CHAIR}','${e.SEMESTER}','${e.DISTYPE}','${e.DCYCLE}','${e.IDZ}','${e.ECOMPONENT}',${e.ELECTIVE ? `'${e.ELECTIVE}'` : `null`},${e.STRNUM ? `'${e.STRNUM}'` : `null`},${e.TASK ? `'${e.TASK}'` : `null`},${e.SELF ? `'${e.SELF}'` : `null`},${e.PRCONTROL ? `'${e.PRCONTROL}'` : `null`},${e.LECTION ? `'${e.LECTION}'` : `null`},${e.LAB ? `'${e.LAB}'` : `null`},${e.PRACT ? `'${e.PRACT}'` : `null`},${e.KONSUL ? `'${e.KONSUL}'` : `null`},${e.SELF2 ? `'${e.SELF2}'` : `null`},${e.TASK2 ? `'${e.TASK2}'` : `null`},${e.SELF4 ? `'${e.SELF4}'` : `null`},${e.TASK4 ? `'${e.TASK4}'` : `null`},${e.CODE ? `'${e.CODE}'` : `null`},${e.NAME ? `'${e.NAME}'` : `null`},${e.ZACH ? `'${e.ZACH}'` : `null`},${e.EXAM ? `'${e.EXAM}'` : `null`},${e.PRACTIVE ? `'${e.PRACTIVE}'` : `null`},${e.DIPLOM ? `'${e.DIPLOM}'` : `null`},${e.GAK ? `'${e.GAK}'` : `null`},${e.ALLHOURSPLUS ? `'${e.ALLHOURSPLUS}'` : `null`},${e.CONS ? `'${e.CONS}'` : `null`},${e.HKURSPR ? `'${e.HKURSPR}'` : `null`},${e.KURSPR ? `'${e.KURSPR}'` : `null`}) `
  });
  query += `SELECT * FROM dual; END;`;
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(query, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'Educational plan updated.' });
    });
  });
});*/
app.post('/eduplan/set', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.plan || !req.body.strings) { res.json({ msg: 'Plan OID/string array required.' }); return; }
  let query = ``;
  req.body.strings.forEach(e => {
    query += `UPDATE ET_DSPLANS SET "EPLAN" = '${req.body.plan}',"DISCIPLINE" = '${e.DISCIPLINE}',"CHAIR" = '${e.CHAIR}',"SEMESTER" = '${e.SEMESTER}',"DISTYPE" = ${e.DISTYPE ? `'${e.DISTYPE}'` : `null`},"DCYCLE" = ${e.DCYCLE ? `'${e.DCYCLE}'` : `null`},"IDZ" = ${e.IDZ ? `'${e.IDZ}'` : `null`},"ECOMPONENT" = ${e.ECOMPONENT ? `'${e.ECOMPONENT}'` : `null`},"ELECTIVE" = ${e.ELECTIVE ? `'${e.ELECTIVE}'` : `null`},"STRNUM" = ${e.STRNUM ? `'${e.STRNUM}'` : `null`},"TASK" = ${e.TASK ? `'${e.TASK}'` : `null`},"SELF" = ${e.SELF ? `'${e.SELF}'` : `null`},"PRCONTROL" = ${e.PRCONTROL ? `'${e.PRCONTROL}'` : `null`},"LECTION" = ${e.LECTION ? `'${e.LECTION}'` : `null`},"LAB" = ${e.LAB ? `'${e.LAB}'` : `null`},"PRACT" = ${e.PRACT ? `'${e.PRACT}'` : `null`},"KONSUL" = ${e.KONSUL ? `'${e.KONSUL}'` : `null`},"SELF2" = ${e.SELF2 ? `'${e.SELF2}'` : `null`},"TASK2" = ${e.TASK2 ? `'${e.TASK2}'` : `null`},"SELF4" = ${e.SELF4 ? `'${e.SELF4}'` : `null`},"TASK4" = ${e.TASK4 ? `'${e.TASK4}'` : `null`},"CODE" = ${e.CODE ? `'${e.CODE}'` : `null`},"NAME" = ${e.NAME ? `'${e.NAME}'` : `null`},"ZACH" = ${e.ZACH ? `'${e.ZACH}'` : `null`},"EXAM" = ${e.EXAM ? `'${e.EXAM}'` : `null`},"PRACTIVE" = ${e.PRACTIVE ? `'${e.PRACTIVE}'` : `null`},"DIPLOM" = ${e.DIPLOM ? `'${e.DIPLOM}'` : `null`},"GAK" = ${e.GAK ? `'${e.GAK}'` : `null`},"ALLHOURSPLUS" = ${e.ALLHOURSPLUS ? `'${e.ALLHOURSPLUS}'` : `null`},"CONS" = ${e.CONS ? `'${e.CONS}'` : `null`} WHERE "OID" = '${e.OID}'; `
  });
  query = `BEGIN ` + query + ` END;`;
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(query, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'Educational plan updated.' });
    });
  });
});
app.post('/eduplan/matrixDisc', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.profile) { res.json({ msg: 'OPOP OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT d.* FROM ET_DISCIPLINES d
    INNER JOIN ET_COMPETENCYCELLS c ON c.DISCIPLINE = d.OID
    INNER JOIN ET_COMPETENCYMATRIX m ON c.MATRIX = m.OID
    INNER JOIN "Training_profile" p ON m.TRAININGPROFILE = p.OID
    WHERE m.MAIN = 1 AND p.OID = '${req.body.profile}'`, function (err, result) {
        if (err) { console.error(err); res.status(500).json({ error: err }); return; }
        connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
        res.json(result.rows);
      });
  });
});

app.post('/chair/list', function (req, res) {
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT * FROM ET_CHAIRS`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.json(result.rows);
    });
  });
});
app.post('/discipline/list', function (req, res) {
  if (!req.body.chair) { res.json({ msg: 'Chair OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT * FROM ET_DISCIPLINES WHERE "CHAIR" = '${req.body.chair}'`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.json(result.rows);
    });
  });
});

app.post('/competency/getCompetencys', function (req, res) {
  /*let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }*/
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    let chair = req.userinfo.CHAIR;
    let query = '';
    if (!req.body.fgos && !req.body.profile)
      query = `SELECT * FROM ET_COMPETENCYS WHERE "TRAININGPROFILE" IS NULL`;
    else if (req.body.fgos)
      query = `SELECT * FROM ET_COMPETENCYS WHERE "FGOS" = '${req.body.fgos}' AND "TRAININGPROFILE" IS NULL`;
    else if (req.body.profile)
      query = `SELECT * FROM ET_COMPETENCYS WHERE "FGOS" = (SELECT FGOS FROM "Training_profile" WHERE "OID" = '${req.body.profile}') AND ("TRAININGPROFILE" = '${req.body.profile}' OR "TRAININGPROFILE" IS NULL)`;
    connection.execute(query, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.json(result.rows);
    });
  });
});
app.post('/competency/competencyTypes', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    let chair = req.userinfo.CHAIR;
    connection.execute(`SELECT * FROM ET_COMPETENCYTYPES`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.json(result.rows);
    });
  });
});

app.post('/competency/matrix/list', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.profile) { res.json({ msg: 'Profile OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT * FROM ET_COMPETENCYMATRIX WHERE "TRAININGPROFILE" = '${req.body.profile}'`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.json(result.rows);
    });
  });
});
app.post('/competency/matrix/get', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.matrix) { res.json({ msg: 'Matrix OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT d.OID AS disciplineoid, c.OID AS competencyoid, d.NAME AS discipline, c.NAME AS competencycode, c.VALUE AS COMPETENCY FROM ET_COMPETENCYCELLS q INNER JOIN ET_DISCIPLINES d ON q.DISCIPLINE = d.OID INNER JOIN ET_COMPETENCYS c ON q.COMPETENCY = c.OID WHERE q.MATRIX = '${req.body.matrix}'`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.json(result.rows);
    });
  });
});
app.post('/competency/matrix/set', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.matrix || !req.body.cells) { res.json({ msg: 'Matrix OID/cell array required.' }); return; }
  let query = `BEGIN DELETE FROM ET_COMPETENCYCELLS WHERE "MATRIX" = '${req.body.matrix}'; INSERT ALL `;
  req.body.cells.forEach(e => {
    query += `INTO ET_COMPETENCYCELLS VALUES (null,'${e.COMPETENCY}','${e.DISCIPLINE}','${req.body.matrix}','${e.FLAG}') `
  });
  query += `SELECT * FROM dual; END;`;
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(query, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'Matrix updated.' });
    });
  });
});
app.post('/competency/matrix/insert', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.profile || !req.body.date || !req.body.description) { res.json({ msg: 'Profile OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`INSERT INTO ET_COMPETENCYMATRIX VALUES (null,'${req.body.profile}','${req.body.date}','${req.body.description}',0)`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'Matrix inserted.' });
    });
  });
});
app.post('/competency/matrix/delete', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.matrix) { res.json({ msg: 'Matrix OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`DELETE FROM ET_COMPETENCYMATRIX WHERE "OID" = '${req.body.matrix}'`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'Matrix deleted.' });
    });
  });
});
app.post('/competency/matrix/main', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Специалист УМУ' || e.NAME == 'Сотрудник кафедры')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.profile || !req.body.matrix) { res.json({ msg: 'Profile/matrix OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`BEGIN
    UPDATE ET_COMPETENCYMATRIX SET "MAIN" = 0 WHERE "TRAININGPROFILE" = '${req.body.profile}';
    UPDATE ET_COMPETENCYMATRIX SET "MAIN" = 1 WHERE "OID" = '${req.body.matrix}';
    END;`, function (err, result) {
        if (err) { console.error(err); res.status(500).json({ error: err }); return; }
        connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
        res.send({ msg: 'Main matrix selected.' });
      });
  });
});

app.post('/admin/competency/insert', function (req, res) {

  if (!req.body.competencytype || !req.body.fgos || !req.body.code || !req.body.name || !req.body.value || !req.body.competencynumber) { res.json({ msg: 'Some info is missing.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`INSERT INTO ET_COMPETENCYS VALUES (null,'${req.body.competencytype}',${req.body.trainingprofile ? `'${req.body.trainingprofile}'` : `null`},'${req.body.fgos}','${req.body.code}','${req.body.name}','${req.body.value}','${req.body.competencynumber}')`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'Competency inserted.' });
    });
  });
});
app.post('/admin/competency/update', function (req, res) {
  if (!req.body.oid || !req.body.competencytype || !req.body.fgos || !req.body.code || !req.body.name || !req.body.value || !req.body.competencynumber) { res.json({ msg: 'Some info is missing.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`UPDATE ET_COMPETENCYS SET "COMPETENCYTYPE" = '${req.body.competencytype}', "TRAININGPROFILE" = ${req.body.trainingprofile ? `'${req.body.trainingprofile}'` : `null`}, "FGOS" = '${req.body.fgos}', "CODE" = '${req.body.code}', "NAME" = '${req.body.name}', "VALUE" = '${req.body.value}', "COMPETENCYNUMBER" = '${req.body.competencynumber}' WHERE "OID" = '${req.body.oid}'`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'Competency updated.' });
    });
  });
});
app.post('/admin/competency/delete', function (req, res) {
  if (!req.body.oid) { res.json({ msg: 'Competency OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`DELETE FROM ET_COMPETENCYS WHERE "OID" = '${req.body.oid}'`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'Competency deleted.' });
    });
  });
});

app.post('/admin/opop/insert', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Администратор ВУЗа')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.profile || !req.body.eform || !req.body.description || !req.body.ver) { res.json({ msg: 'Some info is missing.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`INSERT INTO ET_OPOP VALUES (null,'${req.body.profile}','${req.body.eform}','${req.body.description}','${req.body.ver}')`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'OPOP inserted.' });
    });
  });
});
app.post('/admin/opop/update', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Администратор ВУЗа')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.profile || !req.body.eform || !req.body.description || !req.body.ver || !req.body.oid) { res.json({ msg: 'Some info is missing.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`UPDATE ET_OPOP SET "TRAININGPROFILE" = '${req.body.profile}', "EFORM" = '${req.body.eform}', "DESCRIPTION" = '${req.body.description}', "VER" = '${req.body.ver}' WHERE "OID" = '${req.body.oid}'`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'OPOP updated.' });
    });
  });
});
app.post('/admin/opop/delete', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Администратор ВУЗа')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.oid) { res.json({ msg: 'OPOP OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`DELETE FROM ET_OPOP WHERE "OID" = '${req.body.oid}'`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'OPOP deleted.' });
    });
  });
});

app.post('/admin/fgos/insert', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Администратор ВУЗа')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.code || !req.body.name || !req.body.qualification) { res.json({ msg: 'Some info is missing.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`INSERT INTO FGOS VALUES (null,'${req.body.code}','${req.body.name}','${req.body.qualification}')`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'FGOS inserted.' });
    });
  });
});
app.post('/admin/fgos/update', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Администратор ВУЗа')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.code || !req.body.name || !req.body.qualification || !req.body.oid) { res.json({ msg: 'Some info is missing.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`UPDATE FGOS SET "CODE" = '${req.body.code}', "NAME" = '${req.body.name}', "QUALIFICATION" = '${req.body.qualification}' WHERE "OID" = '${req.body.oid}'`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'FGOS updated.' });
    });
  });
});
app.post('/admin/fgos/delete', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Администратор ВУЗа')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.oid) { res.json({ msg: 'FGOS OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`DELETE FROM FGOS WHERE "OID" = '${req.body.oid}'`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'FGOS deleted.' });
    });
  });
});

app.post('/admin/profile/insert', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Администратор ВУЗа')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.fgos || !req.body.chair || !req.body.chairman || !req.body.profilename || !req.body.programtype || !req.body.profilever) { res.json({ msg: 'Some info is missing.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`INSERT INTO "Training_profile" VALUES (null,'${req.body.fgos}','${req.body.chair}','${req.body.chairman}','${req.body.profilename}','${req.body.programtype}','${req.body.profilever}')`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'Training profile inserted.' });
    });
  });
});
app.post('/admin/profile/update', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Администратор ВУЗа')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.fgos || !req.body.chair || !req.body.chairman || !req.body.profilename || !req.body.programtype || !req.body.profilever || !req.body.oid) { res.json({ msg: 'Some info is missing.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`UPDATE "Training_profile" SET "FGOS" = '${req.body.fgos}', "CHAIR" = '${req.body.chair}', "CHAIRMAN" = '${req.body.chairman}', "PROFILENAME" = '${req.body.profilename}', "PROGRAMTYPE" = '${req.body.programtype}', "PROFILEVER" = '${req.body.profilever}' WHERE "OID" = '${req.body.oid}'`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'Training profile updated.' });
    });
  });
});
app.post('/admin/profile/delete', function (req, res) {
  let access = false;
  req.userinfo.roles.forEach(e => {
    if (e.NAME == 'Администратор ВУЗа')
      access = true;
  });
  if (!access) { res.status(403).send("You don't have access to use this method."); return; }
  if (!req.body.oid) { res.json({ msg: 'Training profile OID required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`DELETE FROM "Training_profile" WHERE "OID" = '${req.body.oid}'`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
      res.send({ msg: 'Training profile deleted.' });
    });
  });
});
/*app.post('/test', function (req, res) {
  if (!req.body.opop) { res.json({ msg: 'OPOP required!' }); return; }
  if (req.userinfo.roleId < 1) { res.status(403).json({ msg: 'You don\'n have permission to use this method.' }); return; }

  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT * FROM TESTWEEK WHERE "id_OPOP" = '${req.body.opop}'`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      if (result.rows.length == 0) {
        let query = `INSERT ALL`;
        for (let index = 0; index < 208; index++)
          query += ` INTO TESTWEEK ("SEMESTER","id_OPOP","num_week","typeeduc_week","mon_type_week","w_type_week","wed_type_week","thu_type_week","fri_type_week","sat_type_week","san_type_week")
           VALUES (${Math.floor((index) / 52) + 1},${req.body.opop},${(index + 1)},0,0,0,0,0,0,0,0)`;
        query += ` SELECT 1 FROM DUAL`;
        connection.execute(query, function (err, result) {
          if (err) { console.error(err); res.status(500).json({ error: err }); return; }
          connection.execute(`SELECT * FROM TESTWEEK WHERE "id_OPOP" = '${req.body.opop}'`, function (err, result) {
            if (err) { console.error(err); res.status(500).json({ error: err }); return; }
            res.json(result.rows);
          })
        });
      }
      else if (result.rows.length == 208) {
        res.json({ rows: result.rows });
      }
      else {
        res.send('Error: invalid row count.');
      }
    });
  });
});*/

app.post('/info', function (req, res) {
  res.json(req.userinfo);
});

app.listen(1313, function () {
  console.log('Express server listening on port 1313');
});

function accessControlHeaders(req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', '*');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', false);

  // Pass to next layer of middleware
  next();
};

function tokenCheck(req, res, next) {
  if (!req.body.token) { res.json({ msg: 'Token required.' }); return; }
  oracledb.getConnection(conn, function (err, connection) {
    if (err) { console.error(err); res.status(500).json({ error: err }); return; }
    connection.execute(`SELECT * FROM ET_USERS WHERE "TOKEN" = '${req.body.token}'`, function (err, result) {
      if (err) { console.error(err); res.status(500).json({ error: err }); return; }
      if (result.rows.length == 0) { res.json({ msg: 'Invalid token.' }); return; }
      req.userinfo = result.rows[0];
      connection.execute(`SELECT r.OID, r.NAME FROM ET_ROLECELLS c INNER JOIN ET_ROLES r ON c.role = r.oid WHERE c."USER" = '${req.userinfo.OID}'`, function (err, result) {
        if (err) { console.error(err); res.status(500).json({ error: err }); return; }
        let arr = [];
        result.rows.forEach(e => {
          arr.push(e);
        });
        connection.release(function (err) { if (err) { console.error(err); res.status(500).json({ error: err }); return; } });
        req.userinfo.roles = arr;
        next();
      });
    });
  });
};